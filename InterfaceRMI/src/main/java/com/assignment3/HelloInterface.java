package com.assignment3;


import java.rmi.Remote;
import java.rmi.RemoteException;


public interface HelloInterface extends Remote {

    //String sayHello(String msg) throws RemoteException;
    String getAll() throws RemoteException;
}
