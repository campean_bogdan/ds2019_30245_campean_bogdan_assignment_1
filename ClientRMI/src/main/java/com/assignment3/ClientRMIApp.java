package com.assignment3;

//import com.assignment3.HelloInterface;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

//@SpringBootApplication
//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class ClientRMIApp {

    private static HelloInterface look_up;


    public static void main(String[] args) {
        //HelloInterface helloService = SpringApplication.run(ClientRMIApp.class, args).getBean("helloInterface", HelloInterface.class);

//        try {
//            Registry registry = LocateRegistry.getRegistry(8084);
//            HelloInterface service = (HelloInterface) registry.lookup("hello");
//            service.sayHello("bogdan");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        try {
            //LocateRegistry.getRegistry(8085);
            String rmi = "rmi://localhost:8085/hello";
            Registry registry = LocateRegistry.getRegistry(8085);

            //look_up = (HelloInterface) Naming.lookup(rmi);
            look_up = (HelloInterface) registry.lookup(rmi);

            //String response = look_up.sayHello("bobu");
            String response = look_up.getAll();

            System.out.println("Client: " + response);
        } catch (Exception e) {
            e.printStackTrace();
        }




//        Client client = new Client();
//
//        client.getButton1().addActionListener(e -> {
//            System.out.println("DA");
//        });


//        String helloObj = null;
//        try {
//            helloObj = service.sayHello("bobu");
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }
//        System.out.println("Client receive: " + helloObj);
    }
}
