package com.assignment3;

import javax.swing.*;

public class Client extends JFrame {
    private JButton button1;

    public Client() {
//        JFrame frame = new JFrame("Client");
//        frame.setContentPane(new Client().panel1);
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame.pack();
//        frame.setVisible(true);
        this.add(panel1);
        this.setSize(500, 500);
        this.setVisible(true);
    }

    private JPanel panel1;


    public JButton getButton1() {
        return button1;
    }

    public void setButton1(JButton button1) {
        this.button1 = button1;
    }

    public JPanel getPanel1() {
        return panel1;
    }

    public void setPanel1(JPanel panel1) {
        this.panel1 = panel1;
    }
}
