package com.assignment1;

import com.assignment1.mapper.SignUpFormMapper;
import com.assignment1.service.*;
import com.assignment1.service.service_impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:application.properties")
public class Config {

    @Autowired
    private Environment env;

    @Bean
    public DataSource getDataSource() {
        DataSource ds = DataSourceBuilder.create()
                .driverClassName(env.getProperty("com.mysql.jdbc.Driver"))
                .url("jdbc:mysql://localhost:3306/medical_application?allowPublicKeyRetrieval=true&autoReconnect=true&useSSL=false")
                .username("root")
                .password("bobu")
                .build();

        return ds;
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() { return new BCryptPasswordEncoder(); }

    @Bean
    public DoctorService doctorService() {
        return new DoctorServiceImpl();
    }

    @Bean
    public PatientService patientService() {
        return new PatientServiceImpl();
    }

    @Bean
    public CaregiverService caregiverService() {
        return new CaregiverServiceImpl();
    }

    @Bean
    public UserService userService() {
        return new UserServiceImpl();
    }

    @Bean
    MedicationService medicationService() { return new MedicationServiceImpl(); }

    @Bean
    public SignUpFormMapper signUpFormMapper() {
        return new SignUpFormMapper();
    }

    @Bean
    public MedicationPlanService medicationPlanService() { return new MedicationPlanServiceImpl(); }

    @Bean
    public ActivityService activityService() { return new ActivityServiceImpl(); }

    // todo: message Converter / jackson
    // todo: RabbitTemplate
}
