package com.assignment1.service;

import com.assignment1.entity.User;
import com.assignment1.form.auth_form.SignUpForm;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface UserService {

    Optional<User> findByUsername(String username);
    Boolean existsByUsername(String username);
    List<User> getAllUsers();
    void insertUserWhenRegister(SignUpForm form);
    void save(User user);
    void deleteUser(String username);
}
