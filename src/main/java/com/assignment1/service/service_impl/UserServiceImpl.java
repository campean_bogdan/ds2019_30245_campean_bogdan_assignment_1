package com.assignment1.service.service_impl;

import com.assignment1.entity.*;
import com.assignment1.form.auth_form.SignUpForm;
import com.assignment1.mapper.SignUpFormMapper;
import com.assignment1.repository.CaregiverRepository;
import com.assignment1.repository.DoctorRepository;
import com.assignment1.repository.PatientRepository;
import com.assignment1.repository.UserRepository;
import com.assignment1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private CaregiverRepository caregiverRepository;
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private SignUpFormMapper mapper;


    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Boolean existsByUsername(String username) {
        User user = userRepository.findByUsername2(username);
        return user != null;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void insertUserWhenRegister(SignUpForm form) {
        switch (form.getRole()) {
            case "ROLE_DOCTOR":
                Doctor doctor = (Doctor)mapper.map(form);
                doctorRepository.save(doctor);
                break;
            case "ROLE_CAREGIVER":
                Caregiver caregiver = (Caregiver)mapper.map(form);
                caregiverRepository.save(caregiver);
                break;
            case "ROLE_PATIENT":
                Patient patient = (Patient)mapper.map(form);
                patientRepository.save(patient);
        }
    }

    @Override
    public void deleteUser(String username) {
        User user = userRepository.findByUsername2(username);
        System.out.println(user.getId() + " " + username);
        if (user.getRole() == Role.ROLE_DOCTOR) {
            userRepository.deleteUser(username);
        }
        if (user.getRole() == Role.ROLE_CAREGIVER) {
            Caregiver caregiver = caregiverRepository.findByUsername(username);
            caregiver.getPatients().clear();
            caregiverRepository.save(caregiver);
            userRepository.deleteUser(username);
        }
        if (user.getRole() == Role.ROLE_PATIENT) {
            Patient patient = patientRepository.findByUsername(username);
            patient.getMedicationPlans().clear();
            patient.getCaregivers().forEach(caregiver -> {
                caregiver.getPatients().clear();
                caregiverRepository.save(caregiver);
            });
            patientRepository.save(patient);
            userRepository.deleteUser(username);
        }
    }
}
