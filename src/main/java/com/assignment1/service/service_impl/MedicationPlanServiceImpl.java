package com.assignment1.service.service_impl;

import com.assignment1.entity.MedicalPlanWithId;
import com.assignment1.entity.Medication;
import com.assignment1.entity.MedicationPlan;
import com.assignment1.entity.Patient;
import com.assignment1.repository.MedicationPlanRepository;
import com.assignment1.repository.MedicationRepository;
import com.assignment1.repository.PatientRepository;
import com.assignment1.service.MedicationPlanService;
import com.assignment1.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MedicationPlanServiceImpl implements MedicationPlanService {

    @Autowired
    private MedicationPlanRepository medicationPlanRepository;
    @Autowired
    private MedicationRepository medicationRepository;
    @Autowired
    private PatientService patientService;
    @Autowired
    private PatientRepository patientRepository;


    @Override
    public Boolean exists(Long id) {
        MedicationPlan medicationPlan = medicationPlanRepository.getOne(id);
        return medicationPlan != null;
    }

    @Override
    public Boolean exists(String name) {
        Medication medication = medicationRepository.findByName(name);
        return medication != null;
    }

    @Override
    public void insertMedicationPlan(MedicationPlan medicationPlan) {
        medicationPlanRepository.save(medicationPlan);
    }

    @Override
    public List<MedicationPlan> getAllMedicationPlans() {
        return medicationPlanRepository.findAll();
    }

    @Override
    public MedicationPlan getDetails(Long id) {
        return medicationPlanRepository.getOne(id);
    }

    @Override
    public void addMedicationToMedicationPlan(String username, MedicalPlanWithId medicalPlanWithId) {
        Patient patient = patientService.findPatientByUsername(username);
        MedicationPlan medicationPlan = new MedicationPlan();

        medicationPlan.setStartDate(medicalPlanWithId.getStartDate());
        medicationPlan.setEndDate(medicalPlanWithId.getEndDate());
        medicationPlan.setIntakeInterval(medicalPlanWithId.getIntakeInterval());
        medicationPlan.setMedications(this.getMedications(medicalPlanWithId));
        medicationPlanRepository.save(medicationPlan);
        patient.addMedicationPlan(medicationPlan);
        patientRepository.save(patient);
    }

    // return a set with medications
    // ids are given by 'medications' field from class MedicalPlanWithId, which comes from frontend
    private Set<Medication> getMedications (MedicalPlanWithId medicalPlanWithId) {
        String[] ids = medicalPlanWithId.getMedications().split(" ");
        Set<Medication> medications = new HashSet<>();
        for (int i = 0; i < ids.length; i++) {
            Long id = Long.parseLong(ids[i]);
            Medication medication = medicationRepository.getOne(id);
            medications.add(medication);
            medication.setDosage(medication.getDosage() - 1);
            medicationRepository.save(medication);
        }
        return medications;
    }
}
