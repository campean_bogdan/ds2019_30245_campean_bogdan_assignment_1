package com.assignment1.service.service_impl;

import com.assignment1.entity.Caregiver;
import com.assignment1.entity.Patient;
import com.assignment1.entity.Role;
import com.assignment1.entity.User;
import com.assignment1.repository.CaregiverRepository;
import com.assignment1.repository.PatientRepository;
import com.assignment1.repository.UserRepository;
import com.assignment1.service.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

public class CaregiverServiceImpl implements CaregiverService {

    @Autowired
    private CaregiverRepository caregiverRepository;
    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private UserRepository userRepository;
    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();


    @Override
    public List<Caregiver> getAllCaregivers() {
        return caregiverRepository.findAll();
    }

    @Override
    public void insertCaregiver(Caregiver caregiver) {
        caregiverRepository.save(caregiver);
    }

    @Override
    public void addPatientToCaregiver(String caregiverUsername, String patientUsername) {
        Caregiver caregiver = caregiverRepository.findByUsername(caregiverUsername);
        Patient patient = patientRepository.findByUsername(patientUsername);
        patient.getCaregivers().add(caregiver);
        caregiver.getPatients().add(patient);
        patientRepository.save(patient);
        caregiverRepository.save(caregiver);
    }

    @Override
    public Caregiver findByUsername(String username) {
        return caregiverRepository.findByUsername(username);
    }

    @Override
    public void editCaregiver(String username, Caregiver caregiver) {
        //Caregiver oldCaregiver = caregiverRepository.getOne(id);
        Caregiver oldCaregiver = caregiverRepository.findByUsername(username);
        map(oldCaregiver, caregiver);
        caregiverRepository.save(oldCaregiver);
    }

    @Override
    public Caregiver getCarregiverDetails(Long id) {
        return caregiverRepository.getOne(id);
    }

    @Override
    public void deleteCaregiver(Long id) {
        caregiverRepository.deleteById(id);
    }

    @Override
    public Boolean exists(String username) {
        Caregiver caregiver = caregiverRepository.findByUsername(username);
        if (caregiver == null)
            return false;
        return true;
    }

    @Override
    public Boolean exists(Long id) {
        User user = userRepository.getOne(id);
        if (user == null)
            return false;
        if (user.getRole() != Role.ROLE_CAREGIVER)
            return false;
        return true;
    }

    private void map(Caregiver newCaregiver, Caregiver caregiver) {
        if (!caregiver.getUsername().isEmpty())
            newCaregiver.setUsername(caregiver.getUsername());
        if (!caregiver.getPassword().isEmpty())
            newCaregiver.setPassword(encoder.encode(caregiver.getPassword()));
        if (!caregiver.getName().isEmpty())
            newCaregiver.setName(caregiver.getName());
        if (!caregiver.getGender().isEmpty())
            newCaregiver.setGender(caregiver.getGender());
        if (!caregiver.getBirthDate().isEmpty())
            newCaregiver.setBirthDate(caregiver.getBirthDate());
        if (!caregiver.getAddress().isEmpty())
            newCaregiver.setAddress(caregiver.getAddress());
    }
}
