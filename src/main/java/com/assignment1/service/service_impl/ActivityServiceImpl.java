package com.assignment1.service.service_impl;

import com.assignment1.entity.Activity;
import com.assignment1.repository.ActivityRepository;
import com.assignment1.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityRepository activityRepository;


    @Override
    public List<Activity> findAll() {
        return activityRepository.findAll();
    }

    @Override
    public void deleteAllActivities() {
        //activityRepository.deleteAll();
        activityRepository.deleteAllActivities();
    }
}
