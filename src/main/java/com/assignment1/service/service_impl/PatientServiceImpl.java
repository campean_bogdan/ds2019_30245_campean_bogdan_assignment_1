package com.assignment1.service.service_impl;

import com.assignment1.entity.MedicationPlan;
import com.assignment1.entity.Patient;
import com.assignment1.entity.Role;
import com.assignment1.entity.User;
import com.assignment1.repository.MedicationPlanRepository;
import com.assignment1.repository.PatientRepository;
import com.assignment1.repository.UserRepository;
import com.assignment1.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;
import java.util.Set;

public class PatientServiceImpl implements PatientService {

    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MedicationPlanRepository medicationPlanRepository;
    @Autowired
    private BCryptPasswordEncoder encoder;


    @Override
    public List<Patient> getAllPatients() {
        return patientRepository.findAll();
    }

    @Override
    public void insertPatient(Patient patient) {
        patient.setPassword(encoder.encode(patient.getPassword()));
        patientRepository.save(patient);
    }

    @Override
    public void updatePatient(Patient patient) {
        patientRepository.save(patient);
    }

    @Override
    public void editPatient(String username, Patient patient) {
        //Patient oldPatient = patientRepository.getOne(id);
        Patient oldPatient = patientRepository.findByUsername(username);
        map(oldPatient, patient);
        patientRepository.save(oldPatient);
    }

    @Override
    public Patient getPatientDetails(Long id) {
        return patientRepository.getOne(id);
    }

    @Override
    public Patient findPatientByUsername(String username) {
        return patientRepository.findByUsername(username);
    }

    @Override
    public void deletePatient(Long id) {
        patientRepository.deleteById(id);
    }

    @Override
    public Set<MedicationPlan> getMedicalRecord(Long id) {
        return patientRepository.getOne(id).getMedicationPlans();
    }

    @Override
    public void addMedicationPlanToPatient(String patientUsername, MedicationPlan medicationPlan) {
//        Patient patient = patientRepository.findByUsername(patientUsername);
//        MedicationPlan medicationPlan = medicationPlanRepository.getOne(medicationPlanId);
//        patient.addMedicationPlan(medicationPlan);
//        patientRepository.save(patient);
        Patient patient = patientRepository.findByUsername(patientUsername);
        patient.addMedicationPlan(medicationPlan);
        patientRepository.save(patient);
    }

    // exista pacient cu username
    @Override
    public Boolean exists(String username) {
        Patient patient = patientRepository.findByUsername(username);
        if (patient == null)
            return false;
        return true;
    }

    // exista user cu id && ROLE_PATIENT
    @Override
    public Boolean exists(Long id) {
        User user = userRepository.getOne(id);
        if (user == null)
            return false;
        if (user.getRole() != Role.ROLE_PATIENT)
            return false;
        return true;
    }

    private void map(Patient newPatient, Patient patient) {
        if (!patient.getUsername().isEmpty())
            newPatient.setUsername(patient.getUsername());
        if (!patient.getPassword().isEmpty())
            newPatient.setPassword(encoder.encode(patient.getPassword()));
        if (!patient.getName().isEmpty())
            newPatient.setName(patient.getName());
        if (!patient.getGender().isEmpty())
            newPatient.setGender(patient.getGender());
        if (!patient.getBirthDate().isEmpty())
            newPatient.setBirthDate(patient.getBirthDate());
        if (!patient.getAddress().isEmpty())
            newPatient.setAddress(patient.getAddress());
    }
}
