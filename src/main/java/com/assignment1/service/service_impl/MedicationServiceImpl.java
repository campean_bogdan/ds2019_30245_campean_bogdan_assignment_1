package com.assignment1.service.service_impl;

import com.assignment1.entity.Medication;
import com.assignment1.repository.MedicationRepository;
import com.assignment1.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class MedicationServiceImpl implements MedicationService {

    @Autowired
    private MedicationRepository medicationRepository;


    @Override
    public List<Medication> getAllMedications() {
        return medicationRepository.findAll();
    }

    @Override
    public void insertMedication(Medication medication) {
        medicationRepository.save(medication);
    }

    @Override
    public void editMedication(String name, Medication medication) {
        Medication oldMedication = medicationRepository.findByName(name);
        map(oldMedication, medication);
        medicationRepository.save(oldMedication);
    }

    @Override
    public void deleteMedication(String name) {
        medicationRepository.deleteMedication(name);
    }

    @Override
    public Medication getMedicationDetails(Long id) {
        return medicationRepository.getOne(id);
    }

    @Override
    public Boolean exists(Long id) {
        Medication medication = medicationRepository.getOne(id);
        return medication != null;
    }

    @Override
    public Boolean exists(String name) {
        Medication medication = medicationRepository.findByName(name);
        return medication != null;
    }

    @Override
    public Boolean existsDosage(Long id, Integer dosage) {
        Medication medication = medicationRepository.getOne(id);
        return medication.getDosage().compareTo(dosage) > 0;
    }

    private void map(Medication oldMedication, Medication medication) {
        if (!medication.getName().isEmpty()) {
            oldMedication.setName(medication.getName());
        }
        if (medication.getDosage() != null) {
            oldMedication.setDosage(medication.getDosage());
        }
        if (!medication.getSideEffects().isEmpty()) {
            oldMedication.setSideEffects(medication.getSideEffects());
        }
    }
}
