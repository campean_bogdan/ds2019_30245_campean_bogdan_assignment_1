package com.assignment1.service.service_impl;

import com.assignment1.entity.Doctor;
import com.assignment1.entity.Role;
import com.assignment1.entity.User;
import com.assignment1.repository.DoctorRepository;
import com.assignment1.repository.UserRepository;
import com.assignment1.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

public class DoctorServiceImpl implements DoctorService {

    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private UserRepository userRepository;
    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();


    @Override
    public void insertDoctor(Doctor doctor) {
        doctor.setPassword(encoder.encode(doctor.getPassword()));
        doctorRepository.save(doctor);
    }

    @Override
    public List<Doctor> getAllDoctors() {
        return doctorRepository.findAll();
    }

    @Override
    public void editDoctor(String username, Doctor doctor) {
        Doctor oldDoctor = doctorRepository.findByUsername(username);
        map(oldDoctor, doctor);
        doctorRepository.save(oldDoctor);
    }

    @Override
    public Doctor getDoctorDetails(Long id) {
        return doctorRepository.getOne(id);
    }

    @Override
    public Doctor getDoctorDetails(String username) {
        return doctorRepository.findByUsername(username);
    }

    @Override
    public Boolean exists(String username) {
        Doctor doctor = doctorRepository.findByUsername(username);
        if (doctor == null)
            return false;
        return true;
    }

    @Override
    public Boolean exists(Long id) {
        User user = userRepository.getOne(id);
        if (user == null)
            return false;
        if (user.getRole() != Role.ROLE_DOCTOR)
            return false;
        return true;
    }

    private void map(Doctor newDoctor, Doctor doctor) {
        if (!doctor.getUsername().isEmpty())
            newDoctor.setUsername(doctor.getUsername());
        if (!doctor.getPassword().isEmpty())
            newDoctor.setPassword(encoder.encode(doctor.getPassword()));
        if (!doctor.getName().isEmpty())
            newDoctor.setName(doctor.getName());
        if (!doctor.getGender().isEmpty())
            newDoctor.setGender(doctor.getGender());
        if (!doctor.getBirthDate().isEmpty())
            newDoctor.setBirthDate(doctor.getBirthDate());
        if (!doctor.getAddress().isEmpty())
            newDoctor.setAddress(doctor.getAddress());
    }
}
