package com.assignment1.service;

import com.assignment1.entity.MedicalPlanWithId;
import com.assignment1.entity.MedicationPlan;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MedicationPlanService {

    void insertMedicationPlan(MedicationPlan medicationPlan);
    List<MedicationPlan> getAllMedicationPlans();
    void addMedicationToMedicationPlan(String username, MedicalPlanWithId medicalPlanWithId);
    Boolean exists(Long id);
    Boolean exists(String name);
    MedicationPlan getDetails(Long id);
}
