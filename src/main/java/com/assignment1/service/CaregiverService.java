package com.assignment1.service;

import com.assignment1.entity.Caregiver;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CaregiverService {

    List<Caregiver> getAllCaregivers();
    void insertCaregiver(Caregiver caregiver);
    void addPatientToCaregiver(String caregiverUsername, String patientUsername);
    Caregiver findByUsername(String username);
    void editCaregiver(String username, Caregiver caregiver);
    Caregiver getCarregiverDetails(Long id);
    void deleteCaregiver(Long id);
    Boolean exists(String username);
    Boolean exists(Long id);
}
