package com.assignment1.service;

import com.assignment1.entity.Activity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ActivityService {

    List<Activity> findAll();
    void deleteAllActivities();
}
