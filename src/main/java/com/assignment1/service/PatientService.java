package com.assignment1.service;

import com.assignment1.entity.MedicationPlan;
import com.assignment1.entity.Patient;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public interface PatientService {

    List<Patient> getAllPatients();
    void insertPatient(Patient patient);
    void editPatient(String username, Patient patient);
    void deletePatient(Long id);
    void updatePatient(Patient patient);
    Set<MedicationPlan> getMedicalRecord(Long id);
    void addMedicationPlanToPatient(String patientUsername, MedicationPlan medicationPlan);
    Patient getPatientDetails(Long id);
    Patient findPatientByUsername(String username);
    Boolean exists(String username);
    Boolean exists(Long id);
}
