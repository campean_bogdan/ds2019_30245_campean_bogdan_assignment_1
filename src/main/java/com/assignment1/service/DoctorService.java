package com.assignment1.service;

import com.assignment1.entity.Doctor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DoctorService {

    void insertDoctor(Doctor doctor);
    List<Doctor> getAllDoctors();
    Doctor getDoctorDetails(Long id);
    Doctor getDoctorDetails(String username);
    void editDoctor(String username, Doctor doctor);
    Boolean exists(String username);
    Boolean exists(Long id);
}
