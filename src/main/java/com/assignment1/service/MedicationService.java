package com.assignment1.service;

import com.assignment1.entity.Medication;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MedicationService {

    List<Medication> getAllMedications();
    void insertMedication(Medication medication);
    void editMedication(String name, Medication medication);
    void deleteMedication(String name);
    Medication getMedicationDetails(Long id);
    Boolean exists(Long id);
    Boolean exists(String name);
    Boolean existsDosage(Long id, Integer dosage);
}
