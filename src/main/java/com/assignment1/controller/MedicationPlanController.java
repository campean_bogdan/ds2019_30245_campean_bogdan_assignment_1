package com.assignment1.controller;

import com.assignment1.api.MedicationPlanAPI;
import com.assignment1.entity.MedicalPlanWithId;
import com.assignment1.entity.MedicationPlan;
import com.assignment1.service.MedicationPlanService;
import com.assignment1.service.MedicationService;
import com.assignment1.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MedicationPlanController implements MedicationPlanAPI {

    @Autowired
    private MedicationPlanService medicationPlanService;
    @Autowired
    private MedicationService medicationService;
    @Autowired
    private PatientService patientService;


    @Override
    public ResponseEntity<List<MedicationPlan>> getAllMedicationPlans() {
        return new ResponseEntity<>(medicationPlanService.getAllMedicationPlans(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> addMedicationToMedicationPlan(String username, MedicalPlanWithId medicalPlanWithId) {
        if (!patientService.exists(username)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        medicationPlanService.addMedicationToMedicationPlan(username, medicalPlanWithId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<MedicationPlan> getMedicationPlanDetails(Long id) {
        if (!medicationPlanService.exists(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(medicationPlanService.getDetails(id), HttpStatus.OK);
    }
}
