package com.assignment1.controller;

import com.assignment1.api.MedicationAPI;
import com.assignment1.entity.Medication;
import com.assignment1.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MedicationController implements MedicationAPI {

    @Autowired
    private MedicationService medicationService;


    @Override
    public ResponseEntity<List<Medication>> getAllMedications() {
        return new ResponseEntity<>(medicationService.getAllMedications(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> insertMedication(Medication medication) {
        if (medicationService.exists(medication.getName())) {
            return new ResponseEntity<>(HttpStatus.FOUND);
        }
        medicationService.insertMedication(medication);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> editMedication(String name, Medication medication) {
//        if (!medicationService.exists(id)) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//        medicationService.editMedication(id, medication);
//        return new ResponseEntity<>(HttpStatus.OK);
        if (!medicationService.exists(name)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        medicationService.editMedication(name, medication);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Medication> getMedicationDetails(Long id) {
        if (!medicationService.exists(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(medicationService.getMedicationDetails(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> deleteMedication(String name) {
        if (!medicationService.exists(name)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        medicationService.deleteMedication(name);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
