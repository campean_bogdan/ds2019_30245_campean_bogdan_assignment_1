package com.assignment1.controller;

import com.assignment1.api.AuthAPI;
import com.assignment1.entity.*;
import com.assignment1.form.auth_form.LoginForm;
import com.assignment1.form.auth_form.SignUpForm;
import com.assignment1.security.jwt.JwtProvider;
import com.assignment1.security.jwt.JwtResponse;
import com.assignment1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class AuthController implements AuthAPI {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserService userService;

    @Autowired
    private JwtProvider jwtProvider;

    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();


    @Override
    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody LoginForm loginRequest) {
        System.out.println("UN: " + loginRequest.getUsername());
        System.out.println("PS: " + loginRequest.getPassword());

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateJwtToken(authentication);
        User user = userService.findByUsername(loginRequest.getUsername()).get();
        JwtResponse jwtResponse = new JwtResponse(jwt, user.getRole());
        return new ResponseEntity<>(jwtResponse, HttpStatus.OK);
    }

    @Override
    @PostMapping("/signUp")
    public ResponseEntity<String> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {
        if (userService.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<>(HttpStatus.FOUND);
        }

        User user = new User();
        String strRole = signUpRequest.getRole();

        switch (strRole) {
            case "ROLE_DOCTOR":
                Role doctorRole = Role.ROLE_DOCTOR;
                user = createDoctor(
                        signUpRequest.getUsername(),
                        signUpRequest.getPassword(),
                        doctorRole,
                        signUpRequest.getName(),
                        signUpRequest.getBirthDate(),
                        signUpRequest.getGender(),
                        signUpRequest.getAddress()
                );
                break;

            case "ROLE_CAREGIVER":
                Role caregiverRole = Role.ROLE_CAREGIVER;
                user = createCaregiver(
                        signUpRequest.getUsername(),
                        signUpRequest.getPassword(),
                        caregiverRole,
                        signUpRequest.getName(),
                        signUpRequest.getBirthDate(),
                        signUpRequest.getGender(),
                        signUpRequest.getAddress()
                );
                break;

            case "ROLE_PATIENT":
                Role patientRole = Role.ROLE_PATIENT;
                user = createPatient(
                        signUpRequest.getUsername(),
                        signUpRequest.getPassword(),
                        patientRole,
                        signUpRequest.getName(),
                        signUpRequest.getBirthDate(),
                        signUpRequest.getGender(),
                        signUpRequest.getAddress()
                );
                break;
        }
        userService.save(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    private User createDoctor(String username, String password, Role doctorRole, String name, String birthDate, String gender, String address) {
        Doctor newDoctor = new Doctor();
        newDoctor.setUsername(username);
        newDoctor.setPassword(encoder.encode(password));
        newDoctor.setRole(doctorRole);
        newDoctor.setName(name);
        newDoctor.setBirthDate(birthDate);
        newDoctor.setGender(gender);
        newDoctor.setAddress(address);
        return newDoctor;
    }

    private User createCaregiver(String username, String password, Role caregiverRole, String name, String birthDate, String gender, String address) {
        User caregiver = new Caregiver();
        caregiver.setUsername(username);
        caregiver.setPassword(encoder.encode(password));
        caregiver.setRole(caregiverRole);
        ((Caregiver) caregiver).setName(name);
        ((Caregiver) caregiver).setAddress(address);
        ((Caregiver) caregiver).setBirthDate(birthDate);
        ((Caregiver) caregiver).setGender(gender);
        return caregiver;
    }

    private User createPatient(String username, String password, Role patientRole, String name, String birthDate, String gender, String address) {
        User patient = new Patient();
        patient.setUsername(username);
        patient.setPassword(encoder.encode(password));
        patient.setRole(patientRole);
        ((Patient) patient).setName(name);
        ((Patient) patient).setAddress(address);
        ((Patient) patient).setBirthDate(birthDate);
        ((Patient) patient).setGender(gender);
        return patient;
    }

}
