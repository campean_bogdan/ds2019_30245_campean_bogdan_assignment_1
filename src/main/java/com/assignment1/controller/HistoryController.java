package com.assignment1.controller;

import com.assignment1.api.HistoryAPI;
import com.assignment1.entity.History;
import com.assignment1.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HistoryController implements HistoryAPI {

    @Autowired
    private HistoryRepository repository;


    @Override
    public ResponseEntity<List<History>> getHistory() {
        return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
    }
}
