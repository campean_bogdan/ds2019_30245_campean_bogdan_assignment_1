package com.assignment1.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@RequestMapping("/public")
public class Controller {

    @GetMapping("/api/test/user")
    @PreAuthorize("hasRole('PATIENT') or hasRole('DOCTOR') or hasRole('CAREGIVER')")
    public String userAccess() {
        return ">>> All users Contents!";
    }

    @GetMapping("/api/test/pm")
    @PreAuthorize("hasRole('ROLE_PATIENT') or hasRole('ROLE_DOCTOR')")
    public String projectManagementAccess() {
        return ">>> Doctor / Patient Contents";
    }

    @GetMapping("/api/test/admin")
    @PreAuthorize("hasRole('DOCTOR')")
    public String adminAccess() {
        return ">>> Doctor Contents";
    }
}
