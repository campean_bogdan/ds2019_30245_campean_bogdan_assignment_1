package com.assignment1.controller;

import com.assignment1.api.CaregiverAPI;
import com.assignment1.entity.Caregiver;
import com.assignment1.entity.Patient;
import com.assignment1.service.CaregiverService;
import com.assignment1.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
public class CaregiverController implements CaregiverAPI {

    @Autowired
    private CaregiverService caregiverService;
    @Autowired
    private PatientService patientService;


    @Override
    public ResponseEntity<String> insertCaregiver(Caregiver caregiver) {
        if (caregiverService.exists(caregiver.getUsername()))
            return new ResponseEntity<>(HttpStatus.FOUND);
        caregiverService.insertCaregiver(caregiver);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Caregiver>> getAllCaregivers() {
        return new ResponseEntity<>(caregiverService.getAllCaregivers(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> addPatientToCaregiver(String caregiverUsername, String patientUsername) {
        Caregiver caregiver = caregiverService.findByUsername(caregiverUsername);
        Patient patient = patientService.findPatientByUsername(patientUsername);
        Set<Caregiver> set = patient.getCaregivers();
        if (set != null)
        if (set.contains(caregiver)) {
            return new ResponseEntity<>(HttpStatus.FOUND);
        }
        caregiverService.addPatientToCaregiver(caregiverUsername, patientUsername);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Caregiver> getCaregiverDetails(Long id) {
        if (!caregiverService.exists(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Caregiver caregiverDetails = caregiverService.getCarregiverDetails(id);
        return new ResponseEntity<>(caregiverDetails, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Caregiver> getCaregiverDetailsByUsername(String username) {
        if (!caregiverService.exists(username)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(caregiverService.findByUsername(username), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> deleteCaregiver(Long id) {
        if (!caregiverService.exists(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        caregiverService.deleteCaregiver(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
