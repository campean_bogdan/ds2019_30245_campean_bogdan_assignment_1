package com.assignment1.controller;

import com.assignment1.api.PatientAPI;
import com.assignment1.entity.MedicationPlan;
import com.assignment1.entity.Patient;
import com.assignment1.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
public class PatientController implements PatientAPI {

    @Autowired
    private PatientService patientService;


    @Override
    public ResponseEntity<List<Patient>> getAllPatients() {
        return new ResponseEntity<>(patientService.getAllPatients(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> insertPatient(Patient patient) {
        if (patientService.exists(patient.getUsername()))
            return new ResponseEntity<>(HttpStatus.FOUND);
        patientService.insertPatient(patient);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Patient> getPatientDetails(Long id) {
        if (!patientService.exists(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Patient patientDetails = patientService.getPatientDetails(id);
        return new ResponseEntity<>(patientDetails, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> deletePatient(Long id) {
        if (!patientService.exists(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        patientService.deletePatient(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> addMedicationPlanToPatient(String username, MedicationPlan medicationPlan) {
        System.out.println("S: " + username);
        if (!patientService.exists(username)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
//        patientService.addMedicationPlanToPatient(patientUsername, medicationPlanId);
//        return new ResponseEntity<>(HttpStatus.OK);
        patientService.addMedicationPlanToPatient(username, medicationPlan);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Patient> getPatientDetailsByUsername(String username) {
        if (!patientService.exists(username)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(patientService.findPatientByUsername(username), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Set<MedicationPlan>> getMedicalRecordForPatient(Long id) {
        if (!patientService.exists(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(patientService.getMedicalRecord(id), HttpStatus.OK);
    }
}
