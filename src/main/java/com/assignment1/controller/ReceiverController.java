package com.assignment1.controller;

import com.assignment1.api.ReceiverAPI;
import com.assignment1.entity.Patient;
import com.assignment1.service.ActivityService;
import com.assignment1.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ReceiverController implements ReceiverAPI {

    @Autowired
    private PatientService patientService;
    @Autowired
    private ActivityService activityService;


    @Override
    public ResponseEntity<List<Patient>> getAllPatients() {
        System.out.println("AICI: " + patientService.getAllPatients());
        return new ResponseEntity<>(patientService.getAllPatients(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Patient> getPatient(Long id) {
        return new ResponseEntity<>(patientService.getPatientDetails(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> UpdatePatient(Patient patient) {
        patientService.updatePatient(patient);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> deleteAllActivities() {
        activityService.deleteAllActivities();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
