package com.assignment1.controller;

import com.assignment1.api.DoctorAPI;
import com.assignment1.entity.*;
import com.assignment1.service.CaregiverService;
import com.assignment1.service.DoctorService;
import com.assignment1.service.PatientService;
import com.assignment1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DoctorController implements DoctorAPI {

    @Autowired
    private DoctorService doctorService;
    @Autowired
    private UserService userService;
    @Autowired
    private CaregiverService caregiverService;
    @Autowired
    private PatientService patientService;
    @Autowired
    private BCryptPasswordEncoder encoder;


    @Override
    public ResponseEntity insertDoctor(Doctor doctor) {
        if (userService.existsByUsername(doctor.getUsername()))
            return new ResponseEntity<>(HttpStatus.FOUND);
        if (doctor.getRole() == Role.ROLE_DOCTOR)
            userService.save(doctor);
        if (doctor.getRole() == Role.ROLE_CAREGIVER) {
            Caregiver caregiver = new Caregiver();
            caregiver.setUsername(doctor.getUsername());
            caregiver.setPassword(encoder.encode(doctor.getPassword()));
            caregiver.setName(doctor.getName());
            caregiver.setAddress(doctor.getAddress());
            caregiver.setBirthDate(doctor.getBirthDate());
            caregiver.setGender(doctor.getGender());
            caregiver.setRole(doctor.getRole());
            userService.save(caregiver);
        }
        if (doctor.getRole() == Role.ROLE_PATIENT) {
            Patient patient = new Patient();
            patient.setUsername(doctor.getUsername());
            patient.setPassword(encoder.encode(doctor.getPassword()));
            patient.setName(doctor.getName());
            patient.setAddress(doctor.getAddress());
            patient.setBirthDate(doctor.getBirthDate());
            patient.setGender(doctor.getGender());
            patient.setRole(doctor.getRole());
            userService.save(patient);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Doctor>> getAllDoctors() {
        return new ResponseEntity<>(doctorService.getAllDoctors(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> editDoctor(String username, Doctor doctor) {
        if (!userService.findByUsername(username).isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        User user = userService.findByUsername(username).get();

        if (user.getRole() == Role.ROLE_DOCTOR) {
            doctorService.editDoctor(username, doctor);
        }
        if (user.getRole() == Role.ROLE_CAREGIVER) {
            Caregiver caregiver = new Caregiver();
            caregiver.setUsername(doctor.getUsername());
            caregiver.setPassword(encoder.encode(doctor.getPassword()));
            caregiver.setName(doctor.getName());
            caregiver.setAddress(doctor.getAddress());
            caregiver.setBirthDate(doctor.getBirthDate());
            caregiver.setGender(doctor.getGender());
            caregiver.setRole(doctor.getRole());
            caregiverService.editCaregiver(username, caregiver);
        }
        if (user.getRole() == Role.ROLE_PATIENT) {
            Patient patient = new Patient();
            patient.setUsername(doctor.getUsername());
            patient.setPassword(encoder.encode(doctor.getPassword()));
            patient.setName(doctor.getName());
            patient.setAddress(doctor.getAddress());
            patient.setBirthDate(doctor.getBirthDate());
            patient.setGender(doctor.getGender());
            patient.setRole(doctor.getRole());
            patientService.editPatient(username, patient);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> deleteUser(String username) {
        System.out.println("USER: " + username);
        if (!userService.findByUsername(username).isPresent()) {
            System.out.println("ERR");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        userService.deleteUser(username);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //@Override
    public ResponseEntity<Doctor> getDoctorDetails(Long id) {
        if (!doctorService.exists(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Doctor doctorDetails = doctorService.getDoctorDetails(id);
        return new ResponseEntity<>(doctorDetails, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Doctor> getDoctorDetails(String username) {
        if (!doctorService.exists(username)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Doctor doctorDetails = doctorService.getDoctorDetails(username);
        return new ResponseEntity<>(doctorDetails, HttpStatus.OK);
    }
}
