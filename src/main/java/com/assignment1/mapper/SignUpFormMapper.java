package com.assignment1.mapper;

import com.assignment1.entity.*;
import com.assignment1.form.auth_form.SignUpForm;

public class SignUpFormMapper {

    public User map(SignUpForm form) {
        switch (form.getRole()) {
            case "ROLE_DOCTOR":
                Doctor doctor = new Doctor();
                doctor.setUsername(form.getUsername());
                doctor.setPassword(form.getPassword());
                doctor.setRole(Role.ROLE_DOCTOR);
                return doctor;
            case "ROLE_CAREGIVER":
                Caregiver caregiver = new Caregiver();
                caregiver.setUsername(form.getUsername());
                caregiver.setPassword(form.getPassword());
                caregiver.setAddress(form.getAddress());
                caregiver.setBirthDate(form.getBirthDate());
                caregiver.setGender(form.getGender());
                caregiver.setName(form.getName());
                return caregiver;
            case "ROLE_PATIENT":
                Patient patient = new Patient();
                patient.setUsername(form.getUsername());
                patient.setPassword(form.getPassword());
                patient.setAddress(form.getAddress());
                patient.setBirthDate(form.getBirthDate());
                patient.setGender(form.getGender());
                patient.setName(form.getName());
                return patient;
        }
        return null;
    }
}
