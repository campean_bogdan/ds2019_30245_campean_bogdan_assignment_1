package com.assignment1.repository;

import com.assignment1.entity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {

    @Transactional
    @Modifying
    @Query("DELETE FROM Activity a WHERE a.id > 0")
    void deleteAllActivities();
}
