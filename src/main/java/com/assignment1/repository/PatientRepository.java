package com.assignment1.repository;

import com.assignment1.entity.Caregiver;
import com.assignment1.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Set;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

    @Transactional
    @Query("select p from Patient p where p.username = ?1 ")
    Patient findByUsername(String username);

    @Modifying
    @Transactional
    @Query("update Patient p set p.caregivers = ?1 where p.id = ?2")
    void updateCaregiver(Set<Caregiver> caregivers, Long id);
}
