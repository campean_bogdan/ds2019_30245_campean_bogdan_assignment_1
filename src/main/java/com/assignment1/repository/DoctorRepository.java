package com.assignment1.repository;

import com.assignment1.entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {

    @Transactional
    @Query("SELECT d FROM Doctor d WHERE d.username = ?1")
    Doctor findByUsername(String username);
}
