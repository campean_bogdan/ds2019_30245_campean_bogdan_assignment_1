package com.assignment1.repository;

import com.assignment1.entity.Caregiver;
import com.assignment1.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Set;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver, Long> {

    @Modifying
    @Transactional
    @Query("update Caregiver c set c.patients = ?1 where c.id = ?2")
    void updateCaregiver(Set<Patient> patients, Long id);

    @Query("select c from Caregiver c where c.username = ?1 ")
    Caregiver findByUsername(String username);
}
