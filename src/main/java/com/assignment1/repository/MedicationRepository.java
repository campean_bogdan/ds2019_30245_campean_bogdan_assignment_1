package com.assignment1.repository;

import com.assignment1.entity.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Long> {

    @Transactional
    @Query("SELECT m FROM Medication m WHERE m.name= ?1")
    Medication findByName(String name);

    @Transactional
    @Modifying
    @Query("DELETE FROM Medication m WHERE m.name = ?1")
    void deleteMedication(String name);
}
