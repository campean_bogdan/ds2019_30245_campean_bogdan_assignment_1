package com.assignment1.receiver;

import com.assignment1.entity.Activity;

import java.util.Date;

class ActivityUtils {

    Boolean checkRule(Activity activity, String action, int minutes) {
        if (activity.getAction().equals(action)) {
            Date startDate = activity.getStartDate();
            Date endDate = activity.getEndDate();
            if (startDate.getDay() == endDate.getDay()) {
                int diffTime = Math.abs(seconds(endDate) - seconds(startDate));
                return diffTime > minutes * 60;
            } else {
                int diffTime = Math.abs(seconds(endDate) + 24 * 3600 - seconds(startDate));
                return diffTime > minutes * 60;
            }
        }
        return false;
    }

    private int seconds(Date date) {
        return date.getHours() * 3600 +
                date.getMinutes() * 60 +
                date.getSeconds();
    }
}
