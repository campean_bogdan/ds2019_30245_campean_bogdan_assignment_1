package com.assignment1.receiver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class WebSocketsController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

//    @MessageMapping("/hello")
//    @SendTo("/queue/hellos")
//    public String sendMessage(@Payload String message) {
//        simpMessagingTemplate.convertAndSendToUser("p1", "/message", message);
//        return message;
//    }

    //@PostMapping("/send-notification")
    public ResponseEntity sendNotification(@RequestBody String message) {
        simpMessagingTemplate.convertAndSendToUser("p1", "/patient/details-by-username", message);
        System.out.println("DA: " + message);
        return new ResponseEntity(HttpStatus.OK);
    }

    @MessageMapping("/send-notification")
    @SendTo("/patient/details-by-username")
    public String greeting(@RequestBody  String message) {
        //simpMessagingTemplate.convertAndSendToUser("p1", "/patient/details-by-username", message);
        System.out.println("AICI: " + message);
        return "Serus";
    }
}
