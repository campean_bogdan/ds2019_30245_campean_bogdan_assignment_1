package com.assignment1.receiver;

import com.assignment1.entity.Activity;
import com.assignment1.entity.Patient;
import com.assignment1.service.PatientService;
import com.assignment1.service.service_impl.PatientServiceImpl;
import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.springframework.web.client.RestTemplate;


public class Receiver {

    private RestTemplate restTemplate = new RestTemplate();
    private static final String url = "http://localhost:8082/receiver/get-patients";
    private static final String GET_PATIENT_BY_ID_URL = "http://localhost:8082/receiver/get-patient/";
    private static final String UPDATE_PATIENT_ACTIVITY = "http://localhost:8082/receiver/update-patient";
    private static final String DELETE_ALL_ACTIVITIES = "http://localhost:8082/receiver/delete-activities";
    private static final String SEND_NOTIFICATION = "http://localhost:8082/app/hello";
    private static final String SEND_NOTIFICATION_TO = "http://localhost:8082/send-notification";


    private static final String HOST_NAME = "localhost";
    private static final String QUEUE_NAME = "Activity";
    private Gson gson = new Gson();

    private PatientService patientService = new PatientServiceImpl();
    private Boolean ok = true;
    private ActivityUtils utils = new ActivityUtils();


    public void startListening() {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(HOST_NAME);
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");
                Activity activity = getDeserialized(message);

                try {
                    System.out.println("RECEIVED MESSAGE: " + message);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // delete activities before inserting others
                if (ok) {
                    restTemplate.delete(DELETE_ALL_ACTIVITIES);
                    ok = false;
                }
                // ---

                // insert activity to patient
                Patient patient = restTemplate.getForObject(GET_PATIENT_BY_ID_URL + activity.getPatientId().toString(), Patient.class);
                patient.addActivity(activity);
                String response = restTemplate.postForObject(UPDATE_PATIENT_ACTIVITY, patient, String.class);

//                String res = restTemplate.postForObject(SEND_NOTIFICATION_TO, "Salutare", String.class);
//                System.out.println("RES: " + res);
                // ---

                // check sleep activity
                if (utils.checkRule(activity, "Sleeping", 10 * 60)) {
                    System.out.println();
                    System.out.println("PROBLEM: " + activity.getStartDate() + "    " + activity.getEndDate() + " " + activity.getAction() + " " + activity.getPatientId());
                    System.out.println();
                }
                // ---

                // check leaving activity
                if (utils.checkRule(activity, "Leaving", 4 * 60)) {
                    System.out.println();
                    System.out.println("PROBLEM: " + activity.getStartDate() + "    " + activity.getEndDate() + " " + activity.getAction() + " " + activity.getPatientId());
                    System.out.println();
                }
                // ---

                // check toileting activity
                if (utils.checkRule(activity, "Toileting", 16)) {
                    System.out.println();
                    System.out.println("PROBLEM: " + activity.getStartDate() + "    " + activity.getEndDate() + " " + activity.getAction() + " " + activity.getPatientId());
                    System.out.println();
                }
                // ---
            };
            channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Activity getDeserialized(String data) {
        return gson.fromJson(data, Activity.class);
    }
}
