package com.assignment1.entity;

public enum Role {
    ROLE_DOCTOR,
    ROLE_CAREGIVER,
    ROLE_PATIENT
}
