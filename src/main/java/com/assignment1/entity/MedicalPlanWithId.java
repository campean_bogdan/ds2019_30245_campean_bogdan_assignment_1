package com.assignment1.entity;

public class MedicalPlanWithId {

    private Long medicationPlanId;
    private String startDate;
    private String endDate;
    private String intakeInterval;
    private String medications;


    public MedicalPlanWithId() {
    }

    public MedicalPlanWithId(String startDate, String endDate, String intakeInterval) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.intakeInterval = intakeInterval;
    }

    public Long getMedicationPlanId() {
        return medicationPlanId;
    }

    public void setMedicationPlanId(Long medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getIntakeInterval() {
        return intakeInterval;
    }

    public void setIntakeInterval(String intakeInterval) {
        this.intakeInterval = intakeInterval;
    }

    public String getMedications() {
        return medications;
    }

    public void setMedications(String medications) {
        this.medications = medications;
    }
}
