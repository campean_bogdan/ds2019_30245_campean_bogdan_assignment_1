package com.assignment1.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Patient extends User {

    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer patientId;
    private String name;
    private String birthDate;
    private String gender;
    private String address;


    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "patient_medication",
            joinColumns = {@JoinColumn(name = "userId")},
            inverseJoinColumns = {@JoinColumn(name = "medicationPlanId")}
    )
    private Set<MedicationPlan> medicationPlans = new HashSet<>();

    @ManyToMany(mappedBy = "patients", cascade = CascadeType.ALL)
    @JsonBackReference
    private Set<Caregiver> caregivers = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "patient_activity",
            joinColumns = {@JoinColumn(name = "patientId")},
            inverseJoinColumns = {@JoinColumn(name = "activityId")}
    )
    private Set<Activity> activities = new HashSet<>();

    public Patient() {}

    public Patient(String name, String birthDate, String gender, String address) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.caregivers = new HashSet<>();
    }

    public Set<Activity> getActivities() {
        return activities;
    }

    public void addActivity(Activity activity) {
        this.activities.add(activity);
    }

    public void setActivities(Set<Activity> activities) {
        this.activities = activities;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(Set<MedicationPlan> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }

    public Set<Caregiver> getCaregivers() {
        return caregivers;
    }

    public void setCaregivers(Set<Caregiver> caregivers) {
        this.caregivers = caregivers;
    }

    public void addMedicationPlan(MedicationPlan medicationPlan) { this.medicationPlans.add(medicationPlan); }
}
