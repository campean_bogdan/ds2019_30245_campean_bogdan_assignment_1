package com.assignment1.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "medications")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long medicationId;
    private String name;
    private String sideEffects;
    private Integer dosage;

//    @ManyToMany(mappedBy = "medications")
//    private List<Doctor> doctors;
//
//    @ManyToMany(mappedBy = "medications")
//    private List<Patient> patients;

    @ManyToMany(mappedBy = "medications", cascade = CascadeType.ALL)
    @JsonBackReference
    private Set<MedicationPlan> medicationPlans;

//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(
//            name = "medication_patient",
//            joinColumns = {@JoinColumn(name = "medicationId")},
//            inverseJoinColumns = {@JoinColumn(name = "patientId")}
//    )
//    private List<Patient> patients;


    public Medication() { }

    public Medication(String name, String sideEffects, Integer dosage) {
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public Set<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(Set<MedicationPlan> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }

    public Long getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(Long medicationId) {
        this.medicationId = medicationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }
}
