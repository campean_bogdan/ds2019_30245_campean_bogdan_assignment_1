package com.assignment1.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "medication_plans", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "medicationPlanId"
        })
})
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long medicationPlanId;
    private String startDate;
    private String endDate;
    private String intakeInterval;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "med_med_plan",
            joinColumns = {@JoinColumn(name = "medicationPlanId")},
            inverseJoinColumns = {@JoinColumn(name = "medicationId")}
    )
    private Set<Medication> medications = new HashSet<>();

    @ManyToMany(mappedBy = "medicationPlans", cascade = CascadeType.ALL)
    @JsonBackReference
    private Set<Patient> patients = new HashSet<>();


    public MedicationPlan() {
    }

    public MedicationPlan(String startDate, String endDate, String intakeInterval) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.intakeInterval = intakeInterval;
    }

    public void addMedication(Medication medication) {
        this.medications.add(medication);
    }

    public Long getMedicationPlanId() {
        return medicationPlanId;
    }

    public void setMedicationPlanId(Long medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
    }

    public Set<Patient> getPatients() {
        return patients;
    }

    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }

    public Set<Medication> getMedications() {
        return medications;
    }

    public void setMedications(Set<Medication> medications) {
        this.medications = medications;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getIntakeInterval() {
        return intakeInterval;
    }

    public void setIntakeInterval(String intakeInterval) {
        this.intakeInterval = intakeInterval;
    }
}
