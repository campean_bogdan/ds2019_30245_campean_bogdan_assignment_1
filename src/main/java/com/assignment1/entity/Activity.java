package com.assignment1.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Activity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long activityId;
    private Long patientId;
    private Date startDate;
    private Date endDate;
    private String action;

    @ManyToMany(mappedBy = "activities", cascade = CascadeType.ALL)
    @JsonBackReference
    private Set<Patient> patients;


    public Activity(Long patientId, Date startDate, Date endDate, String action, Set<Patient> patients) {
        this.patientId = patientId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.action = action;
        this.patients = patients;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public Activity() {}

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Set<Patient> getPatients() {
        return patients;
    }

    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return startDate.toString() + " " +
                endDate.toString() + " " +
                action;
    }
}
