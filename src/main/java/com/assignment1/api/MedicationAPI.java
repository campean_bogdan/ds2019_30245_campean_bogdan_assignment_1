package com.assignment1.api;

import com.assignment1.entity.Medication;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:8083")
@RestController
@RequestMapping("/medication")
public interface MedicationAPI {

    @GetMapping("/medications")
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    ResponseEntity<List<Medication>> getAllMedications();

    @PostMapping("/insert")
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    ResponseEntity<?> insertMedication(@RequestBody Medication medication);

    @PatchMapping("/edit")
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    ResponseEntity<?> editMedication(@Param("name")String name, @RequestBody Medication medication);

    @DeleteMapping("/delete")
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    ResponseEntity<?> deleteMedication(@Param("name") String name);

    @GetMapping("/details/{id}")
    @PreAuthorize("hasRole('ROLE_DOCTOR') or hasRole('ROLE_CAREGIVER') or hasRole('ROLE_PATIENT')")
    ResponseEntity<Medication> getMedicationDetails(@PathVariable("id") Long id);
}
