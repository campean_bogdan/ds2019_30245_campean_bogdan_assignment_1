package com.assignment1.api;

import com.assignment1.entity.Caregiver;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:8083")
@RequestMapping("/caregiver")
@PreAuthorize("hasRole('ROLE_DOCTOR') or hasRole('ROLE_CAREGIVER')")
public interface CaregiverAPI {

    @PostMapping("/insert")
    ResponseEntity<String> insertCaregiver(@RequestBody Caregiver caregiver);

    @GetMapping("/caregivers")
    ResponseEntity<List<Caregiver>> getAllCaregivers();

    @PatchMapping("/add-patient")
    ResponseEntity<?> addPatientToCaregiver(@Param("caregiverUsername") String caregiverUsername, @Param("patientUsername") String patientUsername);

    @GetMapping("/details/{id}")
    ResponseEntity<Caregiver> getCaregiverDetails(@PathVariable("id")Long id);

    @GetMapping("/details-by-username")
    ResponseEntity<Caregiver> getCaregiverDetailsByUsername(@Param("username") String username);

    @DeleteMapping("/delete/{id}")
    ResponseEntity<String> deleteCaregiver(@PathVariable("id") Long id);
}
