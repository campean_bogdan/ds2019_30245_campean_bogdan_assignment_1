package com.assignment1.api;

import com.assignment1.entity.Patient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/receiver")
public interface ReceiverAPI {

    @GetMapping("/get-patients")
    ResponseEntity<List<Patient>> getAllPatients();

    @GetMapping("/get-patient/{id}")
    ResponseEntity<Patient> getPatient(@PathVariable("id") Long id);

    @PostMapping("/update-patient")
    ResponseEntity<?> UpdatePatient(@RequestBody Patient patient);

    @DeleteMapping("/delete-activities")
    ResponseEntity<?> deleteAllActivities();
}
