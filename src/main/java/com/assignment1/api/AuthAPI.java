package com.assignment1.api;

import com.assignment1.entity.User;
import com.assignment1.form.auth_form.LoginForm;
import com.assignment1.form.auth_form.SignUpForm;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:8083")
@RestController
@RequestMapping("/auth")
public interface AuthAPI {

    @PostMapping("/login")
    ResponseEntity<?> login(@Valid @RequestBody LoginForm loginRequest);

    @PostMapping("/signUp")
    ResponseEntity<String> registerUser(@Valid @RequestBody SignUpForm signUpRequest);

    @GetMapping("/users")
    List<User> getAllUsers();
}