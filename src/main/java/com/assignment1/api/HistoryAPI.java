package com.assignment1.api;

import com.assignment1.entity.History;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/history")
public interface HistoryAPI {

    @GetMapping("/get")
    ResponseEntity<List<History>> getHistory();
}