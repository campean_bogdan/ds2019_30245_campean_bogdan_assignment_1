package com.assignment1.api;

import com.assignment1.entity.MedicationPlan;
import com.assignment1.entity.Patient;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "http://localhost:8083")
@RequestMapping("/patient")
public interface PatientAPI {

    @PostMapping("/insert")
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    ResponseEntity<?> insertPatient(@RequestBody Patient patient);

    @GetMapping("/patients")
    @PreAuthorize("hasRole('ROLE_DOCTOR') or hasRole('ROLE_CAREGIVER')")
    ResponseEntity<List<Patient>> getAllPatients();

    @GetMapping("/details/{id}")
    @PreAuthorize("hasRole('ROLE_DOCTOR') or hasRole('ROLE_CAREGIVER') or hasRole('ROLE_PATIENT')")
    ResponseEntity<Patient> getPatientDetails(@PathVariable("id")Long id);

    @GetMapping("/details-by-username")
    @PreAuthorize("hasRole('ROLE_DOCTOR') or hasRole('ROLE_CAREGIVER') or hasRole('ROLE_PATIENT')")
    ResponseEntity<Patient> getPatientDetailsByUsername(@Param("username") String username);

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    ResponseEntity<?> deletePatient(@PathVariable("id") Long id);

    @PostMapping("/add-medication-plan")@PreAuthorize("hasRole('ROLE_DOCTOR')")

    ResponseEntity<?> addMedicationPlanToPatient(@Param("username") String username, @RequestBody MedicationPlan medicationPlan);

    @GetMapping("/medical-record/{id}")
    @PreAuthorize("hasRole('ROLE_DOCTOR') or hasRole('ROLE_CAREGIVER') or hasRole('ROLE_PATIENT')")
    ResponseEntity<Set<MedicationPlan>> getMedicalRecordForPatient(@PathVariable("id") Long id);
}
