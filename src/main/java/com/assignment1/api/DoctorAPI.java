package com.assignment1.api;

import com.assignment1.entity.Doctor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:8083")
@RequestMapping("/doctor")
@PreAuthorize("hasRole('ROLE_DOCTOR')")
public interface DoctorAPI {

    @PostMapping("/insert")
    ResponseEntity<?> insertDoctor(@RequestBody Doctor doctor);

    @GetMapping("/doctors")
    ResponseEntity<List<Doctor>> getAllDoctors();

    @PatchMapping("/edit/{username}")
    ResponseEntity<?> editDoctor(@PathVariable("username") String username, @RequestBody Doctor doctor);

    @GetMapping("/details-by-id/{id}")
    ResponseEntity<Doctor> getDoctorDetails(@PathVariable("id") Long id);

    @GetMapping("/details-by-username/{username}")
    ResponseEntity<Doctor> getDoctorDetails(@PathVariable("username") String username);

    @DeleteMapping("/delete")
    ResponseEntity<?> deleteUser(@Param("username") String username);
}
