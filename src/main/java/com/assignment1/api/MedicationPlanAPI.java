package com.assignment1.api;

import com.assignment1.entity.MedicalPlanWithId;
import com.assignment1.entity.MedicationPlan;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:8083")
@RestController
@RequestMapping("/medication-plan")
public interface MedicationPlanAPI {

    @GetMapping("/medication-plans")
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    ResponseEntity<List<MedicationPlan>> getAllMedicationPlans();

    @GetMapping("/details/{id}")
    @PreAuthorize("hasRole('ROLE_DOCTOR') or hasRole('ROLE_CAREGIVER') or hasRole('ROLE_PATIENT')")
    ResponseEntity<MedicationPlan> getMedicationPlanDetails(@PathVariable("id") Long id);

//    @PostMapping("/insert-to-patient/{username}")
//    ResponseEntity<?> insertMedicationPlan(@PathVariable("username") String username, @RequestBody MedicationPlan medicationPlan);

    @PatchMapping("/add-medication")
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    ResponseEntity<?> addMedicationToMedicationPlan(@Param("username") String username, @RequestBody MedicalPlanWithId medicalPlanWithId);
}
