package com.assignment1;

import com.assignment1.receiver.Receiver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@PropertySource("classpath:application.properties")
public class ServerApp {

	public static void main(String[] args) {

		SpringApplication.run(ServerApp.class, args);
		//SpringApplication.run(ServerRMI.class, args);

		// start RabbitMQ queue
		Receiver receiver = new Receiver();
		receiver.startListening();

		// start socket
	}
}
