package com.assignment1.security.jwt;

import com.assignment1.security.SecurityConst;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtProvider.class);


    public String generateJwtToken(Authentication authentication) {

        UserPrinciple userPrincipal = (UserPrinciple) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject((userPrincipal.getUsername()))
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + SecurityConst.EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS256, SecurityConst.JWT_SECRET)
                .compact();
    }

    public static String generateJwtToken() {
        return Jwts.builder()
                .setSubject("superuser")
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + SecurityConst.EXPIRATION_TIME_FAST))
                .signWith(SignatureAlgorithm.HS256, SecurityConst.JWT_SECRET)
                .compact();
    }

    String getUserNameFromJwtToken(String token) {
        return Jwts.parser()
                .setSigningKey(SecurityConst.JWT_SECRET)
                .parseClaimsJws(token)
                .getBody().getSubject();
    }

    boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(SecurityConst.JWT_SECRET).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature -> Message: {} ", e);
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token -> Message: {}", e);
        } catch (ExpiredJwtException e) {
            logger.error("Expired JWT token -> Message: {}", e);
        } catch (UnsupportedJwtException e) {
            logger.error("Unsupported JWT token -> Message: {}", e);
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty -> Message: {}", e);
        }

        return false;
    }
}