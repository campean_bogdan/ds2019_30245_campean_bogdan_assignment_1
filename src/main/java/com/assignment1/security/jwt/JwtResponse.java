package com.assignment1.security.jwt;

import com.assignment1.entity.Role;

public class JwtResponse {
    private String token;
    private String type = "Bearer";
    private Role role;

    public JwtResponse(String accessToken, Role role) {
        this.token = accessToken;
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getAccessToken() {
        return token;
    }

    public void setAccessToken(String accessToken) {
        this.token = accessToken;
    }

    public String getTokenType() {
        return type;
    }

    public void setTokenType(String tokenType) {
        this.type = tokenType;
    }
}