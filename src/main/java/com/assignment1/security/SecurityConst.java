package com.assignment1.security;

public final class SecurityConst {

    public static final String AUTH_LOGIN_URL = "/login";
    public static final String JWT_SECRET = "{6c129e270c6d45c5ajrfjhu4htg9458hjfoi4398fh43of43iohrnfr9348rfh34oinr89f34h890rf34oifnj0348hrf84390hn304ofn34in002a9580700a19a}";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "secure-api";
    public static final String TOKEN_AUDIENCE = "secure-app";
    public static final Integer EXPIRATION_TIME = 60 * 60 * 1000;
    public static final Integer EXPIRATION_TIME_FAST = 5000;

    private SecurityConst() {
        throw new IllegalStateException("Cannot create instance of static util class");
    }
}
