import React from 'react';
import * as DOCTOR_API from '../api/doctor-api'
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';
import './../styles/RegisterPage.css';
import * as AUTH_API from '../api/auth-api.js'
import * as MEDICATION_API from '../api/medication-api';
import * as CAREGIVER_API from '../api/caregiver-api';


export class onInsertPatientToCaregiver extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            caregiverUsername: '',
            patientUsername: '',
        }

        this.onCaregiverUsernameChange = this.onCaregiverUsernameChange.bind(this);
        this.onPatientUsernameChange = this.onPatientUsernameChange.bind(this);

        this.onInsertPatientToCaregiver = this.onInsertPatientToCaregiver.bind(this);
        this.validateFields = this.validateFields.bind(this);
    }

    onCaregiverUsernameChange(event) {
        this.setState({caregiverUsername: event.target.value})
    }

    onPatientUsernameChange(event) {
        this.setState({patientUsername: event.target.value})
    }

    validateFields(event, form) {
        if (this.state.caregiverUsername.length === 0) {
            alert('\'Caregiver username\' must not be empty!')
            return false;
        } else
        if (this.state.patientUsername.length === 0) {
            alert('\'Patient username\' must not be empty!')
            return false;
        } 
        return true;
    }

    onInsertPatientToCaregiver(event) {
        if (this.validateFields(event) === true) {
            return CAREGIVER_API.addPatientToCaregiver(
                this.state.caregiverUsername,
                this.state.patientUsername,
                (result, status, error) => {
                    if (result !== null && status === 200) {
                        alert('Medication \'' + result.name + '\' was added successfully !')
                    }
                }
            )
        }
    }

    render() {
        return (
        <>

        <form class="registerForm">
            <label>Caregiver username </label><br/>
            <input value={this.state.caregiverUsername} onChange={this.onCaregiverUsernameChange} type="text"/><br/>

            <label>Patient username </label><br/>
            <input value={this.state.patientUsername} onChange={this.onPatientUsernameChange} type="text"/><br/>

            <button class="registerButton" onClick={(event) => this.onInsertPatientToCaregiver(event)}>Insert</button>
        </form>
        </>
        );
    }
}