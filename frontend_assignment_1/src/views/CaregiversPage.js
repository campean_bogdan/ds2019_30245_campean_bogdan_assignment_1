import React from 'react';
import * as DOCTOR_API from '../api/doctor-api';
import * as jwt from '../jwt';
import * as App from '../App';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import LoginPage from './LoginPage';
import '../styles/RegisterPage.css';
import {Redirect} from 'react-router-dom';

import { AllDoctors } from './AllDoctors';
import { DoctorUserDetails } from './DoctorUserDetails';
import InsertUser from './InsertUser.js';
import * as DoctorDetails from './AnotherDoctorDetails'
import * as CaregiverDetails from './AnotherCaregiverDetails';
import * as PatientDetails from './AnotherPatientDetails';
import * as MedicationPlanDetails from './AnotherMedicationPlan'

import { AllCaregivers } from './AllCaregivers';
import { AllPatients } from './AllPatients';
import {EditUser} from './EditUser';
import {AllMedications} from './AllMedications';
import {InsertMedication} from './InsertMedication';
import {InsertMedicationPlan} from './InsertMedicationPlan';
import {DeleteUser} from './DeleteUser';

import '../styles/LinkSection.css'
import { DeleteMedication } from './DeleteMedication';
import { EditMedication } from './EditMedication';
import { AnotherPatientDetails, AnotherMedicationPlan } from './AnotherMedicationPlan';
import { onInsertPatientToCaregiver } from './InsertPatientToCaregiver';
import {CaregiverPage} from './CaregiverPage';
import { CaregiverAssociatedPatients } from './CaregiverAssociatedPatients';



export class CaregiversPage extends React.Component {
    constructor(props) {
        super(props);

        this.logout = this.logout.bind(this);
    }

    logout(event) {
       event.preventDefault();
       window.location.reload(<LoginPage/>);
       jwt.logout();
       this.props.history.push('/');
    }

    render() {
        // redirect to correct page
        let path = <Route exact path="/caregiver/details"><Redirect to="/caregiver/details-by-username"/></Route>

        return (
            <Router>
                <section>
                    {path}

                    <button className="logoutButton" onClick={(event) => this.logout(event)}>Logout</button>
                    <Link className="button" to="/caregiver/details-by-username">Home</Link>
                    <Link className="button" to="/patient/patients">All patients</Link>
                    <Link className="button" to="/caregiver/my-patients">My patients</Link>
                    
                    <Route exact path="/caregiver/details-by-username" component={CaregiverPage}/>
                    <Route exact path="/patient/patients" component={AllPatients}/>
                    <Route exact path="/patient/details/:id" component={PatientDetails.SearchPage}/>
                    <Route exact path="/caregiver/my-patients" component={CaregiverAssociatedPatients}/>
                    <Route exact path="/medication-plan/details/:id" component={MedicationPlanDetails.SearchPage}/>
                </section>
            </Router>
        )
    }
}