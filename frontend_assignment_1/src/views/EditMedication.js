import React from 'react';
import * as DOCTOR_API from '../api/doctor-api'
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';
import './../styles/RegisterPage.css';
import * as AUTH_API from '../api/auth-api.js'
import * as MEDICATION_API from '../api/medication-api';


export class EditMedication extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            sideEffects: '',
            dosage: '',
        }

        this.onNameChange = this.onNameChange.bind(this);
        this.onSideEffectsChange = this.onSideEffectsChange.bind(this);
        this.onDosageChange = this.onDosageChange.bind(this);

        this.onEditMedication = this.onEditMedication.bind(this);
        this.validateFields = this.validateFields.bind(this);
    }

    onNameChange(event) {
        this.setState({name: event.target.value})
    }

    onSideEffectsChange(event) {
        this.setState({sideEffects: event.target.value})
    }

    onDosageChange(event) {
        this.setState({dosage: event.target.value})
    }

    validateFields(form) {
        return true;
    }

    onEditMedication(event) {
        let newMedication = {
            name: this.state.name,
            sideEffects: this.state.sideEffects,
            dosage: this.state.dosage,
        }

            return MEDICATION_API.editMedication(
                newMedication,
                (result, status, error) => {
                    console.log("Response: " + status)
                    if (result !== null && status === 200) {
                        alert("Medication \'" + newMedication.name + "\' was edited successfully !")
                    }
                }
            )

    }

    render() {
        localStorage.getItem('token')
        return (
        <>

        <form class="registerForm">
            <label>Name </label><br/>
            <input value={this.state.name} onChange={this.onNameChange} type="text"/><br/>
            <label>Side effects </label><br/>
            <textarea value={this.state.sideEffects} onChange={this.onSideEffectsChange} type="password"/><br/>
            <label>Dosage</label>
            <input value={this.state.dosage} onChange={this.onDosageChange} type="text"></input>

            <button class="registerButton" onClick={(event) => this.onEditMedication(event)}>Edit</button>
        </form>
        </>
        );
    }
}