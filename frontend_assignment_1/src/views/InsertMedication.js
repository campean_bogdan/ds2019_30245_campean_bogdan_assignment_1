import React from 'react';
import * as DOCTOR_API from '../api/doctor-api'
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';
import './../styles/RegisterPage.css';
import * as AUTH_API from '../api/auth-api.js'
import * as MEDICATION_API from '../api/medication-api';


export class InsertMedication extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            sideEffects: '',
            dosage: ''
        }

        this.onNameChange = this.onNameChange.bind(this);
        this.onSideEffectsChange = this.onSideEffectsChange.bind(this);
        this.onDosageChange = this.onDosageChange.bind(this);

        this.onInsertMedication = this.onInsertMedication.bind(this);
        this.validateFields = this.validateFields.bind(this);
    }

    onNameChange(event) {
        this.setState({name: event.target.value})
    }

    onSideEffectsChange(event) {
        this.setState({sideEffects: event.target.value})
    }

    onDosageChange(event) {
        this.setState({dosage: event.target.value})
    }

    validateFields(event, form) {
        if (form.name.length === 0) {
            alert('\'Name\' must not be emtpy!')
            return false;
        } else
        if (form.sideEffects.length === 0) {
            alert('\'Side effects\' must not be emtpy!')
            return false;
        } else
        if (form.dosage.length === 0) {
            alert('\'Dosage\' Date must not be emtpy!')
            return false;
        }
        return true;
    }

    onInsertMedication(event) {
        let newMedication = {
            name: this.state.name,
            sideEffects: this.state.sideEffects,
            dosage: this.state.dosage,
        }

        if (this.validateFields(event, newMedication) === true) {
            return MEDICATION_API.insertMedication(
                newMedication,
                (result, status, error) => {
                    if (result !== null && status === 200) {
                        alert('Medication \'' + result.name + '\' was added successfully !')
                    }
                }
            )
        }
    }

    render() {
        return (
        <>

        <form class="registerForm">
            <label>Name </label><br/>
            <input value={this.state.name} onChange={this.onNameChange} type="text"/><br/>

            <label>Side effects </label><br/>
            <textarea value={this.state.sideEffects} onChange={this.onSideEffectsChange} type="password"/><br/>

            <label>Dosage</label>
            <input value={this.state.dosage} onChange={this.onDosageChange} type="text"></input>
            <button class="registerButton" onClick={(event) => this.onInsertMedication(event)}>Insert</button>
        </form>
        </>
        );
    }
}