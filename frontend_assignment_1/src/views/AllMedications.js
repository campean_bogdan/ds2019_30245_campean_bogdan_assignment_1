import React from 'react';
import * as DOCTOR_API from '../api/doctor-api';
import * as CAREGIVER_API from '../api/caregiver-api';
import * as PATIENT_API from '../api/patient-api';
import * as MEDICATION_API from '../api/medication-api';
import * as jwt from '../jwt';
import * as App from '../App';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import LoginPage from './LoginPage';
import './../styles/TableStyle.css'



export class AllMedications extends React.Component {
    constructor(props) {
        super(props);

        this.data = [];

        this.getAllMedications = this.getAllMedications.bind(this);
        this.renderTable = this.renderTable.bind(this);
    }

    renderHeader() {
        let header = [
            <>
        <th>Id</th>
        <th>Name</th> 
        <th>Side effects</th>
        <th>Dosage</th> 
        </>
    ]
        return header;
    }

    renderTable() {
        return this.data.map((doctor, index) => {
            const {id, name, sideEffects, dosage} = doctor
            return (
                <tr key={id}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{sideEffects}</td>
                    <td>{dosage}</td>
                </tr>
            )
        })
    }

    componentWillMount() {
        this.getAllMedications();
    }

    getAllMedications() {
        return MEDICATION_API.getAllMedications(
            (result, status, error) => {
                if (result !== null && status === 200) {
                    result.forEach(medication => {
                        this.data.push({
                            id: medication.medicationId,
                            name: medication.name,
                            sideEffects: medication.sideEffects,
                            dosage: medication.dosage,
                        })
                    });
                    this.forceUpdate();
                } else {
                    
                }
            }
        )
    }

    render() {
        
        return (
            <>
                <h1><u>All medications</u></h1>

                <table id='medications'>
                    <tbody>
                        <tr>{this.renderHeader()}</tr>
                        {this.renderTable()}
                    </tbody>
                </table>

            </>
        );
    }

    
}