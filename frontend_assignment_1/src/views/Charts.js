import React from 'react';
import * as DOCTOR_API from '../api/doctor-api';
import * as ACTIVITY_API from '../api/activity-api';
import * as jwt from '../jwt';
import * as App from '../App';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import LoginPage from './LoginPage';
import './../styles/TableStyle.css'
import * as PATIENT_API from '../api/patient-api';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {CreateChart} from './CreateChart';

import Chart from "react-apexcharts";
import CanvasJSReact from './canvasjs.react';
import moment from 'moment';
import { duration } from '@material-ui/core/styles';
//var CanvasJSReact = require('./canvasjs.react');
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;


let path = '';

const SearchPage = ({match, location}) => {
    path = location.pathname;
    return (
        <Charts/>
    )
}


class Charts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activityId: '',
            action: '',
            startDate: '',
            endDate: '',
            patientId: '',
            path: path
        }

        this.data = [];
        this.sleepingData = [];
        this.toiletingData = [];
        this.showeringData = [];
        this.breakfastData = [];
        this.groomingData = [];
        this.spareTimeData = [];
        this.leavingData = [];
        this.lunchData = [];
        this.snackData = [];

        this.goToDetails = this.goToDetails.bind(this);
        this.getActivities = this.getActivities.bind(this);

        this.getActivities();
    }

    componentDidMount() {
        this.getActivities();
        console.log("CDM" + this.state.path)
    }

    getActivities() {
        let id = 911;

        console.log("P: " + this.state.path)

        return ACTIVITY_API.getActivitiesByPatientId(
            this.state.path,
            //id,
            (result, status, error) => {
                if (result !== null && status === 200) {
                    result.forEach(activity => {

                        if (activity.action === "Sleeping") {
                            let startDate = new Date(activity.startDate)
                            let mSD = moment(activity.startDate)
                            let mED = moment(activity.endDate)
                            const diff = mED.diff(mSD);
                            const duration = moment.duration(diff);
                            console.log(activity.action)
                            
                            this.sleepingData.push({ x: startDate, y: duration.asHours() })
                        }
                        if (activity.action === "Toileting") {
                            let startDate = new Date(activity.startDate)
                            let mSD = moment(activity.startDate)
                            let mED = moment(activity.endDate)
                            const diff = mED.diff(mSD);
                            const duration = moment.duration(diff);
                            console.log(activity.action)

                            this.toiletingData.push({ x: startDate, y: duration.asMinutes() })
                        }
                        if (activity.action === "Showering") {
                            let startDate = new Date(activity.startDate)
                            let mSD = moment(activity.startDate)
                            let mED = moment(activity.endDate)
                            const diff = mED.diff(mSD);
                            const duration = moment.duration(diff);
                            console.log(activity.action)

                            this.showeringData.push({ x: startDate, y: duration.asMinutes() })
                        }
                        if (activity.action === "Breakfast") {
                            let startDate = new Date(activity.startDate)
                            let mSD = moment(activity.startDate)
                            let mED = moment(activity.endDate)
                            const diff = mED.diff(mSD);
                            const duration = moment.duration(diff);
                            console.log(activity.action)

                            this.breakfastData.push({ x: startDate, y: duration.asMinutes() })
                        }
                        if (activity.action === "Grooming") {
                            let startDate = new Date(activity.startDate)
                            let mSD = moment(activity.startDate)
                            let mED = moment(activity.endDate)
                            const diff = mED.diff(mSD);
                            const duration = moment.duration(diff);
                            console.log(activity.action)

                            this.groomingData.push({ x: startDate, y: duration.asMinutes() })
                        }
                        if (activity.action === "Spare_Time/TV") {
                            let startDate = new Date(activity.startDate)
                            let mSD = moment(activity.startDate)
                            let mED = moment(activity.endDate)
                            const diff = mED.diff(mSD);
                            const duration = moment.duration(diff);
                            console.log(activity.action)

                            this.spareTimeData.push({ x: startDate, y: duration.asHours() })
                        }
                        if (activity.action === "Leaving") {
                            let startDate = new Date(activity.startDate)
                            let mSD = moment(activity.startDate)
                            let mED = moment(activity.endDate)
                            const diff = mED.diff(mSD);
                            const duration = moment.duration(diff);
                            console.log(activity.action)

                            this.leavingData.push({ x: startDate, y: duration.asHours() })
                        }
                        if (activity.action === "Lunch") {
                            let startDate = new Date(activity.startDate)
                            let mSD = moment(activity.startDate)
                            let mED = moment(activity.endDate)
                            const diff = mED.diff(mSD);
                            const duration = moment.duration(diff);
                            console.log(activity.action)

                            this.lunchData.push({ x: startDate, y: duration.asMinutes() })
                        }
                        if (activity.action === "Snack") {
                            let startDate = new Date(activity.startDate)
                            let mSD = moment(activity.startDate)
                            let mED = moment(activity.endDate)
                            const diff = mED.diff(mSD);
                            const duration = moment.duration(diff);
                            console.log(activity.action)

                            this.snackData.push({ x: startDate, y: duration.asSeconds() })
                        }
                    });
                    this.forceUpdate();
                }
            }
        )
    }

    goToDetails(e) {
        alert("CLICK")
    }

        render() {
        const sleepingOptions = {
                    animationEnabled: true,
                    exportEnabled: true,
                    theme: "light2", //"light1", "dark1", "dark2"
                    title:{
                        text: "Sleeping"
                    },
                    scales: {
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: "Hours"
                            }
                        }]
                    },
                    data: [{
                        type: "column", //change type to bar, line, area, pie, etc
                        //indexLabel: "{y}", //Shows y value on all Data Points
                        indexLabelFontColor: "#5A5757",
                        indexLabelPlacement: "outside",
                        dataPoints: this.sleepingData
                    }]
                }
                
                const toiletingOptions = {
                    animationEnabled: true,
                    exportEnabled: true,
                    theme: "light2", //"light1", "dark1", "dark2"
                    title:{
                        text: "Toileting"
                    },
                    data: [{
                        type: "column", //change type to bar, line, area, pie, etc
                        //indexLabel: "{y}", //Shows y value on all Data Points
                        indexLabelFontColor: "#5A5757",
                        indexLabelPlacement: "outside",
                        dataPoints: this.toiletingData
                    }]
                }

                const showeringOptions = {
                    animationEnabled: true,
                    exportEnabled: true,
                    theme: "light2", //"light1", "dark1", "dark2"
                    title:{
                        text: "Showering"
                    },
                    data: [{
                        type: "column", //change type to bar, line, area, pie, etc
                        //indexLabel: "{y}", //Shows y value on all Data Points
                        indexLabelFontColor: "#5A5757",
                        indexLabelPlacement: "outside",
                        dataPoints: this.showeringData
                    }]
                }

                const breakfastOptions = {
                    animationEnabled: true,
                    exportEnabled: true,
                    theme: "light2", //"light1", "dark1", "dark2"
                    title:{
                        text: "Breakfast"
                    },
                    data: [{
                        type: "column", //change type to bar, line, area, pie, etc
                        //indexLabel: "{y}", //Shows y value on all Data Points
                        indexLabelFontColor: "#5A5757",
                        indexLabelPlacement: "outside",
                        dataPoints: this.breakfastData
                    }]
                }

                const groomingOptions = {
                    animationEnabled: true,
                    exportEnabled: true,
                    theme: "light2", //"light1", "dark1", "dark2"
                    title:{
                        text: "Grooming"
                    },
                    data: [{
                        type: "column", //change type to bar, line, area, pie, etc
                        //indexLabel: "{y}", //Shows y value on all Data Points
                        indexLabelFontColor: "#5A5757",
                        indexLabelPlacement: "outside",
                        dataPoints: this.groomingData
                    }]
                }

                const spareTimeOptions = {
                    animationEnabled: true,
                    exportEnabled: true,
                    theme: "light2", //"light1", "dark1", "dark2"
                    title:{
                        text: "Spare Time/TV"
                    },
                    data: [{
                        type: "column", //change type to bar, line, area, pie, etc
                        //indexLabel: "{y}", //Shows y value on all Data Points
                        indexLabelFontColor: "#5A5757",
                        indexLabelPlacement: "outside",
                        dataPoints: this.spareTimeData
                    }]
                }

                const leavingOptions = {
                    animationEnabled: true,
                    exportEnabled: true,
                    theme: "light2", //"light1", "dark1", "dark2"
                    title:{
                        text: "Leaving"
                    },
                    data: [{
                        type: "column", //change type to bar, line, area, pie, etc
                        //indexLabel: "{y}", //Shows y value on all Data Points
                        indexLabelFontColor: "#5A5757",
                        indexLabelPlacement: "outside",
                        dataPoints: this.leavingData
                    }]
                }

                const lunchOptions = {
                    animationEnabled: true,
                    exportEnabled: true,
                    theme: "light2", //"light1", "dark1", "dark2"
                    title:{
                        text: "Lunch"
                    },
                    data: [{
                        type: "column", //change type to bar, line, area, pie, etc
                        //indexLabel: "{y}", //Shows y value on all Data Points
                        indexLabelFontColor: "#5A5757",
                        indexLabelPlacement: "outside",
                        dataPoints: this.lunchData
                    }]
                }

                const snackOptions = {
                    animationEnabled: true,
                    exportEnabled: true,
                    theme: "light2", //"light1", "dark1", "dark2"
                    title:{
                        text: "Snack"
                    },
                    data: [{
                        type: "column", //change type to bar, line, area, pie, etc
                        //indexLabel: "{y}", //Shows y value on all Data Points
                        indexLabelFontColor: "#5A5757",
                        indexLabelPlacement: "outside",
                        dataPoints: this.snackData
                    }]
                }
                
                return (
                <div>
                    <CanvasJSChart options = {sleepingOptions} />
                    <CanvasJSChart options = {toiletingOptions} />
                    <CanvasJSChart options = {showeringOptions} />
                    <CanvasJSChart options = {breakfastOptions} />
                    <CanvasJSChart options = {groomingOptions} />
                    <CanvasJSChart options = {spareTimeOptions} />
                    <CanvasJSChart options = {leavingOptions} />
                    <CanvasJSChart options = {lunchOptions} />
                    <CanvasJSChart options = {snackOptions} />
                    {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
                </div>
                );
            }
    
}

export {
    SearchPage,
    Charts
}