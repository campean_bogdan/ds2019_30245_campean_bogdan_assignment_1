import React from 'react';
import * as DOCTOR_API from '../api/doctor-api';
import * as CAREGIVER_API from '../api/caregiver-api';
import * as jwt from '../jwt';
import * as App from '../App';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import LoginPage from './LoginPage';
import './../styles/TableStyle.css'


export class AllCaregivers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            name: '',
            address: '',
            birthDate: '',
            gender: ''
        }

        this.data = [];

        this.getAllCaregivers = this.getAllCaregivers.bind(this);
        this.renderTable = this.renderTable.bind(this);
    }

    renderHeader() {
        let header = [
            <>
        <th>Id</th>
        <th>Name</th> 
        <th>Username</th>
        <th>Address</th> 
        <th>Birth Date</th>
        <th>Gender</th>
        </>
    ]
        return header;
    }

    renderTable() {
        return this.data.map((doctor, index) => {
            const {id, name, username, address, birthDate, gender} = doctor
            return (
                <tr key={id}>
                    <Link to={`/caregiver/details/${id}`}>{id}</Link>
                    <td>{name}</td>
                    <td>{username}</td>
                    <td>{address}</td>
                    <td>{birthDate}</td>
                    <td>{gender}</td>
                </tr>
            )
        })
    }

    componentWillMount() {
        this.getAllCaregivers();
    }

    getAllCaregivers() {
        return CAREGIVER_API.getAllCaregivers(
            (result, status, error) => {
                if (result !== null && status === 200) {
                    result.forEach(caregiver => {
                        console.log("HERE " + caregiver.patients)
                        this.data.push({
                            id: caregiver.id,
                            name: caregiver.name,
                            username: caregiver.username,
                            address: caregiver.address,
                            birthDate: caregiver.birthDate,
                            gender: caregiver.gender,

                        })
                    });
                    this.forceUpdate();
                } else {
                    
                }
            }
        )
    }

    render() {
        
        return (
            <>
                <h1><u>All caregivers</u></h1>

                <table id='caregivers'>
                    <tbody>
                        <tr>{this.renderHeader()}</tr>
                        {this.renderTable()}
                    </tbody>
                </table>

            </>
        );
    }

    
}