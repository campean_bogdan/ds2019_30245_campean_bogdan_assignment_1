import React from 'react';
import * as DOCTOR_API from '../api/doctor-api';
import * as jwt from '../jwt';
import * as App from '../App';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import LoginPage from './LoginPage';
import '../styles/RegisterPage.css';
import {Redirect} from 'react-router-dom';

import { AllDoctors } from './AllDoctors';
import { DoctorUserDetails } from './DoctorUserDetails';
import InsertUser from './InsertUser.js';
import * as DoctorDetails from './AnotherDoctorDetails'
import * as CaregiverDetails from './AnotherCaregiverDetails';
import * as PatientDetails from './AnotherPatientDetails';
import * as MedicationPlanDetails from './AnotherMedicationPlan'

import { AllCaregivers } from './AllCaregivers';
import { AllPatients } from './AllPatients';
import {EditUser} from './EditUser';
import {AllMedications} from './AllMedications';
import {InsertMedication} from './InsertMedication';
import {InsertMedicationPlan} from './InsertMedicationPlan';
import {DeleteUser} from './DeleteUser';

import '../styles/LinkSection.css'
import { DeleteMedication } from './DeleteMedication';
import { EditMedication } from './EditMedication';
import { AnotherMedicationPlan } from './AnotherMedicationPlan';
import {AnotherPatientDetails} from './AnotherPatientDetails';
import { onInsertPatientToCaregiver } from './InsertPatientToCaregiver';
import {CaregiverPage} from './CaregiverPage';
import { CaregiverAssociatedPatients } from './CaregiverAssociatedPatients';
import { PatientPage } from './PatientPage';
import * as PatientDet from './PatientMP';



export class PatientsPage extends React.Component {
    constructor(props) {
        super(props);

        this.logout = this.logout.bind(this);
    }

    logout(event) {
       event.preventDefault();
       window.location.reload(<LoginPage/>);
       jwt.logout();
       this.props.history.push('/');
    }

    render() {
        // redirect to correct page
        let path = <Route exact path="/patient/details"><Redirect to="/patient/details-by-username"/></Route>

        return (
            <Router>
                <section>
                    {path}

                    <button className="logoutButton" onClick={(event) => this.logout(event)}>Logout</button>
                    <Link className="button" to="/patient/details-by-username">Home</Link>
                    
                    <Route exact path="/medication-plan/details/:id" component={MedicationPlanDetails.SearchPage}/>
                    <Route exact path="/patient/details-by-username" component={PatientDet.SearchPage}/>
                </section>
            </Router>
        )
    }
}