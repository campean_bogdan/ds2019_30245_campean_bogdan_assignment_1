import React from 'react';
import * as DOCTOR_API from '../api/doctor-api'
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';
import './../styles/RegisterPage.css';
import * as AUTH_API from '../api/auth-api.js'


export class DeleteUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: ''
        }

        this.onUsernameChange = this.onUsernameChange.bind(this);
        this.onDeleteUser = this.onDeleteUser.bind(this);
        this.validateFields = this.validateFields.bind(this);
    }

    onUsernameChange(event) {
        this.setState({username: event.target.value})
        event.preventDefault();
    }

    validateFields() {
        if (this.state.username.length === 0) {
            alert('\'Username\' must not be empty!')
            return false;
        } 
        return true;
    }

    onDeleteUser(event) {
        if (this.validateFields() === true) {
            return DOCTOR_API.deleteUser(
                this.state.username,
                (result, status, error) => {
                    if (result !== null && status === 200) {
                        alert('User \'' + result.name + '\' was deleted successfully !')
                    }
                }
            )
        }
    }

    render() {
        localStorage.getItem('token')
        return (
        <>

        <form class="registerForm">
            <label>Username </label><br/>
            <input value={this.state.username} onChange={(event) => this.onUsernameChange(event)} type="text"/><br/>
            <button class="registerButton" onClick={(event) => this.onDeleteUser(event)}>Delete</button>
        </form>
        </>
        );
    }
}