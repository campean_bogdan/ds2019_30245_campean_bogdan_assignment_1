import React from 'react';
import * as DOCTOR_API from '../api/doctor-api'
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';
import './../styles/RegisterPage.css';
import * as AUTH_API from '../api/auth-api.js'
import * as MEDICATION_API from '../api/medication-api';
import '../styles/LinkSection.css'
import '../styles/RegisterPage.css';
import '../styles/TableStyle.css';


export class InsertMedicationPlan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            patientName: '',
            startDate: '',
            endDate: '',
            intakeInterval: '',
            medications: ''
        }

        this.data = [];

        this.onNameChange = this.onNameChange.bind(this);
        this.onSideEffectsChange = this.onSideEffectsChange.bind(this);
        this.onDosageChange = this.onDosageChange.bind(this);
        this.getAllMedications = this.getAllMedications.bind(this);
        this.onMedicationsChange = this.onMedicationsChange.bind(this);
        this.onIntakeInterval = this.onIntakeInterval.bind(this);

        this.onInsertMedicationPlan = this.onInsertMedicationPlan.bind(this);
        this.validateFields = this.validateFields.bind(this);
    }

    onNameChange(event) {
        this.setState({patientName: event.target.value})
        event.preventDefault();
    }

    onSideEffectsChange(event) {
        this.setState({startDate: event.target.value})
        event.preventDefault()
    }

    onDosageChange(event) {
        this.setState({endDate: event.target.value})
        event.preventDefault();
    }

    onMedicationsChange(event) {
        this.setState({medications: event.target.value})
        event.preventDefault();
    }

    onIntakeInterval(event) {
        this.setState({intakeInterval: event.target.value})
        event.preventDefault();
    }

    validateFields(form) {
        if (this.state.patientName.length === 0) {
            alert('\'Name\' must not be emtpy!')
            return false;
        }
        return true;
    }

    componentDidMount() {
        this.getAllMedications();
    }

    getAllMedications() {
        return MEDICATION_API.getAllMedications(
            (result, status, error) => {
                if (result !== null && status === 200) {
                    result.forEach(medication => {
                        this.data.push({
                            id: medication.medicationId,
                            name: medication.name,
                            sideEffects: medication.sideEffects,
                            dosage: medication.dosage,
                        })
                    });
                    this.forceUpdate();
                } else {
                    alert("Medications not found");
                }
            }
        )
    }

    onInsertMedicationPlan(event) {
        let newMedicationWithId = {
            startDate: this.state.startDate,
            endDate: this.state.endDate,
            intakeInterval: this.state.intakeInterval,
            medications: this.state.medications,
        }

        if (this.validateFields(newMedicationWithId) === true) {
            return MEDICATION_API.addMedicationPlan(
                this.state.patientName,
                newMedicationWithId,
                (result, status, error) => {
                    if (result !== null && status === 200) {
                        alert('Medication \'' + result.name + '\' was added successfully !')
                    }
                    if (status === 404) {
                        alert('Username or medication does not exist !')
                    }
                }
            )
        }
    }

    renderHeader() {
        let header = [
            <>
        <th>Id</th>
        <th>Name</th> 
        <th>Side effects</th>
        <th>Dosage</th> 
        </>
    ]
        return header;
    }

    renderTable() {
        return this.data.map((medications, index) => {
            const {id, name, sideEffects, dosage} = medications
            return (
                <tr key={id}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{sideEffects}</td>
                    <td>{dosage}</td>
                </tr>
            )
        })
    }

    render() {
        return (
        <>
        <form class="medicationPlanForm">
            <label>Patient username</label><br/>
            <input value={this.state.patientName} onChange={(event) => this.onNameChange(event)} type="text"/><br/>

            <label>Start Date</label><br/>
            <input value={this.state.sideEffects} onChange={(event) => this.onSideEffectsChange(event)} type="date"/><br/>

            <label>End Date</label><br/>
            <input value={this.state.dosage} onChange={(event) => this.onDosageChange(event)} type="date"></input><br></br>

            <label>Intake interval</label><br></br>
            <textarea value={this.state.intakeInterval} onChange={(event) => this.onIntakeInterval(event)} type="text"></textarea>

            <label>Insert medications (by id with white spaces)</label><br></br>
            <label>Ex: 12 23 11 4</label><br></br>
            <input value={this.state.medications} onChange={(event) => this.onMedicationsChange(event)}></input><br></br>

            <button class="registerButton" onClick={(event) => this.onInsertMedicationPlan(event)}>Insert medication plan</button>
        </form>

        <h1><u>All medications</u></h1>

                <table className="medTable" id='medications'>
                    <tbody>
                        <tr>{this.renderHeader()}</tr>
                        {this.renderTable()}
                    </tbody>
                </table>
        </>
        );
    }
}