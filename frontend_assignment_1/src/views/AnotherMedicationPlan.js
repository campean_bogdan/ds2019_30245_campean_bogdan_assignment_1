import React from 'react';
import * as DOCTOR_API from '../api/doctor-api';
import * as CAREGIVER_API from '../api/caregiver-api';
import * as PATIENT_API from '../api/patient-api';
import * as MEDICATION_API from '../api/medication-api';


let path = '';

const SearchPage = ({match, location}) => {
    path = location.pathname;
    return (
        <AnotherMedicationPlan/>
    )
}

class AnotherMedicationPlan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            startDate: '',
            endDate: '',
            intakeInterval: '',
            medications: [],
            // path = vine ca si request
            path: path
        }

        this.getMedicalPlanDetails = this.getMedicalPlanDetails.bind(this);
    }

    componentDidMount() {
        this.getMedicalPlanDetails();
    }

    getMedicalPlanDetails() {
        console.log("PATH: " + this.state.path);
        return MEDICATION_API.getMedicalPlanDetails(
            this.state.path,
            (result, status, error) => {
                if (result != null && status === 200) {
                    console.log("HERE " + result.patients);
                    this.setState({
                        id: result.medicationPlanId,
                        startDate: result.startDate,
                        endDate: result.endDate,
                        intakeInterval: result.intakeInterval,
                        medications: result.medications
                    })
                }
            }
        )
    }

    renderHeader() {
        let header = [
            <>
        <th>Id</th>
        <th>Name</th> 
        <th>Side effects</th>
        <th>Dosage</th> 
        </>
    ]
        return header;
    }

    renderTable() {
        return this.state.medications.map((medication, index) => {
            const {medicationId, name, sideEffects, dosage} = medication
            return (
                <tr key={medicationId}>
                    <td>{medicationId}</td>
                    <td>{name}</td>
                    <td>{sideEffects}</td>
                    <td>{dosage}</td>
                </tr>
            )
        })
    }

    render() {
        return (
            <>
            <h1><u>Medication Plan Details</u></h1>
            <div>
            <h3>Medication Plan Id: {this.state.id} </h3>
            <h3>Start date: {this.state.startDate}</h3>
            <h3>End date: {this.state.endDate}</h3>
            <h3>Intake interval: {this.state.intakeInterval}</h3>
            </div>

            <h2>Medications in medication plan: </h2>

            <table id='patients'>
                    <tbody>
                        <tr>{this.renderHeader()}</tr>
                        {this.renderTable()}
                    </tbody>
                </table>
            </>
        )
    }

}

export {
    SearchPage,
    AnotherMedicationPlan
}