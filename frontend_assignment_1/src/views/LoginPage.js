import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';
import './../styles/LoginPage.css';
import * as AUTH_API from './../api/auth-api.js';
import {setToken} from './../jwt';
import {Redirect} from 'react-router-dom';
import * as jwt from './../jwt';
import RegisterPage from './RegisterPage';
import { DoctorsPage } from './DoctorsPage';
import { CaregiverPage } from './CaregiverPage';
import { PatientPage } from './PatientPage';


class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            role: ''
        }

        this.onUsernameChange = this.onUsernameChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.login = this.login.bind(this);
        this.validateFields = this.validateFields.bind(this);
    }

    onUsernameChange(event) {
        event.preventDefault();
        this.setState({username: event.target.value})
        console.log('Username: ' + this.state.username)
    }

    onPasswordChange(event) {
        event.preventDefault();
        this.setState({password: event.target.value})
        console.log('Password ' + this.state.password);
    }

    validateFields(form) {
        if (form.username.length === 0) {
            alert('\'Username\' must not be empty!')
            return false;
        } else
        if (form.password.length === 0) {
            alert('\'Password\' must not be empty!')
            return false;
        } 
        return true;
    }

    onSubmit(event) {
        let loginForm = {
            username: this.state.username,
            password: this.state.password
        }
        
        if (this.validateFields(loginForm) === true) {
            this.login(loginForm);
        }

        event.preventDefault();
    }

    componentDidMount() {
        
    }

    login(loginForm) {
        AUTH_API.login(
            loginForm,
            (result, status, error) => {
                if (result !== null && status === 200) {
                    localStorage.setItem("token", result.accessToken);
                    localStorage.setItem("username", loginForm.username);
                    localStorage.setItem("password", loginForm.password);
                    this.state.role = result.role;
                    if (this.state.role === 'ROLE_DOCTOR') {
                        window.location.reload(<DoctorsPage/>);
                        jwt.loginAsDoctor();
                        alert('Login successfully as DOCTOR !')
                        this.props.history.push('/doctor');
                    } else
                    if (this.state.role === 'ROLE_CAREGIVER') {
                        window.location.reload(<CaregiverPage/>);
                        jwt.loginAsCaregiver();
                        alert('Login successfully as CAREGIVER !')
                        this.props.history.push('/caregiver')
                    } else
                    if (this.state.role === 'ROLE_PATIENT') {
                        window.location.reload(<PatientPage/>)
                        jwt.loginAsPatient();
                        alert('Login successfully as PATIENT !')
                        this.props.history.push('/patient')
                    }
                } else
                if (status === 401) { 
                    alert('User does not exist !')
                }
            }
        )
    }

    render() {
        return (
            <>
            <form class="loginForm" onSubmit={(event) => this.onSubmit(event)}>
                <label>Username </label><br/>
                <input value={this.state.username} onChange={this.onUsernameChange} type="text"/><br/>
                <label>Password </label><br/>
                <input value={this.state.password} onChange={this.onPasswordChange} type="password"/><br/>
                <button class="loginButton">Login</button>
            </form>
            </>
        );
    }
};

export default LoginPage;