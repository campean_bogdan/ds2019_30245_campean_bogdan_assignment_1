import React from 'react';
import * as DOCTOR_API from '../api/doctor-api';
import * as jwt from '../jwt';
import * as App from '../App';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import LoginPage from './LoginPage';
import '../styles/RegisterPage.css';
import {Redirect} from 'react-router-dom';

import { AllDoctors } from './AllDoctors';
import { DoctorUserDetails } from './DoctorUserDetails';
import InsertUser from './InsertUser.js';
import * as DoctorDetails from './AnotherDoctorDetails'
import * as CaregiverDetails from './AnotherCaregiverDetails';
import * as PatientDetails from './AnotherPatientDetails';
import * as MedicationPlanDetails from './AnotherMedicationPlan'

import { AllCaregivers } from './AllCaregivers';
import { AllPatients } from './AllPatients';
import {EditUser} from './EditUser';
import {AllMedications} from './AllMedications';
import {InsertMedication} from './InsertMedication';
import {InsertMedicationPlan} from './InsertMedicationPlan';
import {DeleteUser} from './DeleteUser';
import * as Ch from './Charts';
import {CreateChart} from './CreateChart';

import '../styles/LinkSection.css'
import { DeleteMedication } from './DeleteMedication';
import { EditMedication } from './EditMedication';
import { AnotherPatientDetails, AnotherMedicationPlan } from './AnotherMedicationPlan';
import { onInsertPatientToCaregiver } from './InsertPatientToCaregiver';


export class DoctorsPage extends React.Component {
    constructor(props) {
        super(props);

        this.logout = this.logout.bind(this);
    }

    logout(event) {
       event.preventDefault();
       window.location.reload(<LoginPage/>);
       jwt.logout();
       this.props.history.push('/');
    }

    render() {
        // redirect to correct page
        let path = <Route exact path="/doctor/details"><Redirect to="/doctor/details-by-username"/></Route>

        return (
            <Router>
                <section>
                    {path}

                    <button className="logoutButton" onClick={(event) => this.logout(event)}>Logout</button>
                    <Link className="button" to="/doctor/details-by-username">Home</Link>
                    <Link className="button" to="/doctor/doctors">All doctors</Link>
                    <Link className="button" to="/caregiver/caregivers">All caregivers</Link>
                    <Link className="button" to="/patient/patients">All patients</Link>
                    <Link className="button" to="/medication/medications">All medications</Link>
                    <Link className="button" to="/doctor/insert">Insert user</Link>                    
                    <Link className="button" to="/medication/insert">Insert medication</Link>
                    <Link className="button" to="/caregiver/add-patient">Insert patient to caregiver</Link>
                    <Link className="button" to="/patient/add-medication-plan">Insert medication plan</Link>
                    <Link className="button" to="/doctor/edit/">Edit user</Link>
                    <Link className="button" to="/medication/edit">Edit medication</Link>
                    <Link className="button" to="/doctor/delete">Delete user</Link>
                    <Link className="button" to="/medication/delete">Delete medication</Link>
                    
                    <Route exact path="/doctor/details-by-username" component={DoctorUserDetails}/>
                    <Route exact path="/doctor/details-by-id/:id" component={DoctorDetails.SearchPage}/>
                    <Route exact path="/doctor/doctors" component={AllDoctors}/>
                    <Route exact path="/doctor/insert" component={InsertUser}/>
                    <Route exact path="/doctor/edit" component={EditUser}/>
                    <Route exact path="/doctor/delete" component={DeleteUser}/>

                    <Route exact path="/caregiver/caregivers" component={AllCaregivers}/>
                    <Route exact path="/caregiver/details/:id" component={CaregiverDetails.SearchPage}/>

                    <Route exact path="/patient/patients" component={AllPatients}/>
                    <Route exact path="/patient/details/:id" component={PatientDetails.SearchPage}/>

                    <Route exact path="/medication/medications" component={AllMedications}/>
                    <Route exact path="/medication/insert" component={InsertMedication}/>
                    <Route exact path="/patient/add-medication-plan" component={InsertMedicationPlan}/>
                    <Route exact path="/medication/delete" component={DeleteMedication}/>
                    <Route exact path="/medication/edit" component={EditMedication}/>
                    <Route exact path="/medication-plan/details/:id" component={MedicationPlanDetails.SearchPage}/>
                    <Route exact path="/caregiver/add-patient" component={onInsertPatientToCaregiver}/>

                    <Link className="button" to="/activity/all-activities">Activity Charts</Link>
                    <Route exact path="/activity/all-activities" component={CreateChart}/>
                    <Route exact path="/activity/all-activities-by-patient/:id" component={Ch.SearchPage}/>
                    
                </section>
            </Router>
        )
    }
}