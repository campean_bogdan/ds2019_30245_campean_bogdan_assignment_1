import React from 'react';
import * as DOCTOR_API from '../api/doctor-api';
import * as jwt from '../jwt';
import * as App from '../App';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import LoginPage from './LoginPage';
import './../styles/TableStyle.css'


export class AllDoctors extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            name: '',
            address: '',
            birthDate: '',
            gender: ''
        }

        this.data = [];

        this.getAllDoctors = this.getAllDoctors.bind(this);
        this.renderTable = this.renderTable.bind(this);
        this.goToDetails = this.goToDetails.bind(this);
    }

    renderHeader() {
        let header = [
            <>
        <th>Id</th>
        <th>Name</th> 
        <th>Username</th>
        <th>Address</th> 
        <th>Birth Date</th>
        <th>Gender</th>
        </>
    ]
        return header;
    }

    renderTable() {
        return this.data.map((doctor, index) => {
            const {id, name, username, address, birthDate, gender} = doctor
            return (
                <tr key={id}>
                    <Link to={`/doctor/details-by-id/${id}`}>{id}</Link>
                    <td>{name}</td>
                    <td>{username}</td>
                    <td>{address}</td>
                    <td>{birthDate}</td>
                    <td>{gender}</td>
                </tr>
            )
        })
    }

    componentWillMount() {
        this.getAllDoctors();
    }

    getAllDoctors() {
        return DOCTOR_API.getAllDoctors(
            (result, status, error) => {
                if (result !== null && status === 200) {
                    result.forEach(doctor => {
                        this.data.push({
                            id: doctor.id,
                            name: doctor.name,
                            username: doctor.username,
                            address: doctor.address,
                            birthDate: doctor.birthDate,
                            gender: doctor.gender
                        })
                    });
                    this.forceUpdate();
                } else {
                    
                }
            }
        )
    }

    goToDetails(e) {
        alert("CLICK")
    }

    render() {
        
        return (
            <>
                <h1><u>All doctors</u></h1>

                <table id='doctors'>
                    <tbody>
                        <tr>{this.renderHeader()}</tr>
                        {this.renderTable()}
                    </tbody>
                </table>

            </>
        );
    }

    
}