import React, { Component } from "react";
import Chart from "react-apexcharts";
import CanvasJSReact from './canvasjs.react';
import {Charts} from './Charts';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import * as PATIENT_API from '../api/patient-api';


export class CreateChart extends Component {
  constructor(props) {
	super(props);
	this.state = {
            username: '',
            name: '',
            address: '',
            birthDate: '',
            gender: ''
        }

        this.data = [];

        this.getAllPatients = this.getAllPatients.bind(this);
        this.renderTable = this.renderTable.bind(this);
   }

    renderHeader() {
        let header = [
            <>
        <th>Id</th>
        <th>Name</th> 
        <th>Username</th>
        <th>Address</th> 
        <th>Birth Date</th>
        <th>Gender</th>
        </>
    ]
        return header;
    }

    renderTable() {
        return this.data.map((doctor, index) => {
            const {id, name, username, address, birthDate, gender} = doctor
            return (
                <tr key={id}>
                    <Link to={`/activity/all-activities-by-patient/${id}`}>{id}</Link>
                    <td>{name}</td>
                    <td>{username}</td>
                    <td>{address}</td>
                    <td>{birthDate}</td>
                    <td>{gender}</td>
                </tr>
            )
        })
    }

   componentWillMount() {
        this.getAllPatients();
    }

    getAllPatients() {
        return PATIENT_API.getAllPatients(
            (result, status, error) => {
                if (result !== null && status === 200) {
                    result.forEach(doctor => {
                        this.data.push({
                            id: doctor.id,
                            name: doctor.name,
                            username: doctor.username,
                            address: doctor.address,
                            birthDate: doctor.birthDate,
                            gender: doctor.gender
                        })
                    });
                    this.forceUpdate();
                } else {
                    
                }
            }
        )
    }

    render() {
        
        return (
            <>
                <h1><u>All patients</u></h1>

                <table id='caregivers'>
                    <tbody>
                        <tr>{this.renderHeader()}</tr>
                        {this.renderTable()}
                    </tbody>
                </table>

            </>
        );
    }
}
