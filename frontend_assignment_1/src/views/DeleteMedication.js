import React from 'react';
import * as DOCTOR_API from '../api/doctor-api'
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';
import './../styles/RegisterPage.css';
import * as AUTH_API from '../api/auth-api.js'
import * as MEDICATION_API from '../api/medication-api'


export class DeleteMedication extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: ''
        }

        this.onNameChange = this.onNameChange.bind(this);
        this.onDeleteMedication = this.onDeleteMedication.bind(this);
        this.validateFields = this.validateFields.bind(this);
    }

    onNameChange(event) {
        this.setState({name: event.target.value})
        event.preventDefault();
    }

    validateFields() {
        if (this.state.name.length === 0) {
            alert('\'Name\' must not be empty!')
            return false;
        } 
        return true;
    }

    onDeleteMedication(event) {
        if (this.validateFields() === true) {
            return MEDICATION_API.deleteMedication(
                this.state.name,
                (result, status, error) => {
                    if (result !== null && status === 200) {
                        alert('User \'' + result.name + '\' was deleted successfully !')
                    }
                }
            )
        }
    }

    render() {
        localStorage.getItem('token')
        return (
        <>

        <form class="registerForm">
            <label>Name </label><br/>
            <input value={this.state.name} onChange={(event) => this.onNameChange(event)} type="text"/><br/>
            <button class="registerButton" onClick={(event) => this.onDeleteMedication(event)}>Delete</button>
        </form>
        </>
        );
    }
}