import React from 'react';
import * as DOCTOR_API from '../api/doctor-api';
import * as CAREGIVER_API from '../api/caregiver-api';


let path = '';

const SearchPage = ({match, location}) => {
    path = location.pathname;
    return (
        <AnotherCaregiverDetails/>
    )
}

class AnotherCaregiverDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            username: '',
            gender: '',
            address: '',
            birthDate: '',
            role: '',
            patients: [],
            // path = vine ca si request
            path: path
        }

        this.getAnotherCaregiverDetails = this.getAnotherCaregiverDetails.bind(this);
    }

    componentDidMount() {
        this.getAnotherCaregiverDetails();
    }

    getAnotherCaregiverDetails() {
        return CAREGIVER_API.getAnotherCaregiverDetails(
            this.state.path,
            (result, status, error) => {
                if (result != null && status === 200) {
                    console.log("HERE " + result.patients);
                    this.setState({
                        name: result.name,
                        username: result.username,
                        gender: result.gender,
                        address: result.address,
                        birthDate: result.birthDate,
                        role: result.role,
                        patients: result.patients
                    })
                }
            }
        )
    }

    renderHeader() {
        let header = [
            <>
        <th>Id</th>
        <th>Name</th> 
        <th>Username</th>
        <th>Address</th> 
        <th>Birth Date</th>
        <th>Gender</th>
        </>
    ]
        return header;
    }

    renderTable() {
        return this.state.patients.map((patient, index) => {
            const {id, name, username, address, birthDate, gender} = patient
            return (
                <tr key={id}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{username}</td>
                    <td>{address}</td>
                    <td>{birthDate}</td>
                    <td>{gender}</td>
                </tr>
            )
        })
    }

    render() {
        return (
            <>
            <h1><u>Caregiver Details</u></h1>
            <div>
            <h3>Name: {this.state.name}</h3>
            <h3>Username: {this.state.username}</h3>
            <h3>Gender: {this.state.gender}</h3>
            <h3>Address: {this.state.address}</h3>
            <h3>Birth Date: {this.state.birthDate}</h3>
            <h3>Role: {this.state.role}</h3>
            </div>

            <h2>Patients: </h2>

            <table id='patients'>
                    <tbody>
                        <tr>{this.renderHeader()}</tr>
                        {this.renderTable()}
                    </tbody>
                </table>
            </>
        )
    }

}

export {
    SearchPage,
    AnotherCaregiverDetails
}