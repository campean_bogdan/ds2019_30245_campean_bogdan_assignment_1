import React from 'react';
import * as DOCTOR_API from '../api/doctor-api'
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';
import './../styles/RegisterPage.css';
import * as AUTH_API from '../api/auth-api.js'


export default class InsertUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            name: '',
            address: '',
            birthDate: '',
            gender: 'Male',
            role: 'ROLE_DOCTOR',
        }

        this.onUsernameChange = this.onUsernameChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
        this.onAddressChange = this.onAddressChange.bind(this);
        this.onBirthDateChange = this.onBirthDateChange.bind(this);
        this.onGenderChange = this.onGenderChange.bind(this);
        this.onRoleChange = this.onRoleChange.bind(this);

        this.onInsertUser = this.onInsertUser.bind(this);
        this.validateFields = this.validateFields.bind(this);
    }

    onUsernameChange(event) {
        this.setState({username: event.target.value})
    }

    onPasswordChange(event) {
        this.setState({password: event.target.value})
    }

    onNameChange(event) {
        this.setState({name: event.target.value})
    }

    onAddressChange(event) {
        this.setState({address: event.target.value})
    }

    onBirthDateChange(event) {
        this.setState({birthDate: event.target.value})
    }

    onGenderChange(event) {
        this.setState({gender: event.target.value})
    }

    onRoleChange(event) {
        this.setState({role: event.target.value})
    }

    validateFields(form) {
        if (form.username.length === 0) {
            alert('\'Username\' must not be empty!')
            return false;
        } else
        if (form.password.length === 0) {
            alert('\'Password\' must not be emtpy!')
            return false;
        } else
        if (form.name.length === 0) {
            alert('\'Name\' must not be emtpy!')
            return false;
        } else
        if (form.address.length === 0) {
            alert('\'Address\' must not be emtpy!')
            return false;
        } else
        if (form.birthDate.length === 0) {
            alert('\'Birth\' Date must not be emtpy!')
            return false;
        }
        return true;
    }

    onInsertUser(event) {
        
        let newUser = {
            username: this.state.username,
            password: this.state.password,
            name: this.state.name,
            address: this.state.address,
            birthDate: this.state.birthDate,
            gender: this.state.gender,
            role: this.state.role
        }

        if (this.validateFields(newUser) === true) {
            return DOCTOR_API.insertUser(
                newUser,
                (result, status, error) => {
                    console.log("insert " + result.username)
                    console.log("here " + status + "  " + result)
                    if (result !== null && status === 200) {
                        alert('User \'' + result.name + '\' was added successfully !')
                    }
                }
            )
        }
    }

    render() {
        localStorage.getItem('token')
        return (
        <>

        <form class="registerForm">
            <label>Username </label><br/>
            <input value={this.state.username} onChange={this.onUsernameChange} type="text"/><br/>
            <label>Password </label><br/>
            <input value={this.state.password} onChange={this.onPasswordChange} type="password"/><br/>
            <label>Name</label>
            <input value={this.state.name} onChange={this.onNameChange} type="text"></input>
            <label>Address</label>
            <input value={this.state.address} onChange={this.onAddressChange} type="text"></input>
            <label>Birth Date</label>
            <input value={this.state.birthDate} onChange={this.onBirthDateChange} type="date" name="bday"/>
            <label>Gender</label><br/>
            <select onChange={this.onGenderChange}>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
            </select>
            <br></br>
            <label>Role</label><br/>
            <select onChange={this.onRoleChange}>
                <option value="ROLE_DOCTOR">DOCTOR</option>
                <option value="ROLE_CAREGIVER">CAREGIVER</option>
                <option value="ROLE_PATIENT">PATIENT</option>
            </select>
            <br></br>
            <button class="registerButton" onClick={(event) => this.onInsertUser(event)}>Insert</button>
        </form>
        </>
        );
    }
}