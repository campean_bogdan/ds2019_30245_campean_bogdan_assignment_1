import React from 'react';
import * as DOCTOR_API from './../api/doctor-api';
import * as CAREGIVER_API from '../api/caregiver-api';
import * as PATIENT_API from '../api/patient-api';


export class PatientPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            username: '',
            gender: '',
            address: '',
            birthDate: ''
        }

        this.getLoggedPatientDetails = this.getLoggedPatientDetails.bind(this);
    }

    componentDidMount() {
        this.getLoggedPatientDetails();
    }

    getLoggedPatientDetails() {
        let username = localStorage.getItem('username')
        console.log("OUT: " + username);
        return PATIENT_API.getPatientDetails(
            username,
            (result, status, error) => {
                if (result != null && status === 200) {
                    this.setState({
                        name: result.name,
                        username: result.username,
                        gender: result.gender,
                        address: result.address,
                        birthDate: result.birthDate
                    })
                }
            }
        )
    }

    render() {
        return (
            <>
            <h1><u>Your Account Details</u></h1>
            <div>
            <h3>Name: {this.state.name}</h3>
            <h3>Username: {this.state.username}</h3>
            <h3>Gender: {this.state.gender}</h3>
            <h3>Address: {this.state.address}</h3>
            <h3>Birth Date: {this.state.birthDate}</h3>
            </div>
            </>
        )
    }

}