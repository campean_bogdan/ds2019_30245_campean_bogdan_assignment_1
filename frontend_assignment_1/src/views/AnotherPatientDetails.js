import React from 'react';
import * as DOCTOR_API from '../api/doctor-api';
import * as CAREGIVER_API from '../api/caregiver-api';
import * as PATIENT_API from '../api/patient-api';
import { Link } from 'react-router-dom';


let path = '';

const SearchPage = ({match, location}) => {
    path = location.pathname;
    return (
        <AnotherPatientDetails/>
    )
}

class AnotherPatientDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            username: '',
            gender: '',
            address: '',
            birthDate: '',
            role: '',
            medicationPlans: [],
            // path = vine ca si request
            path: path
        }

        this.getAnotherPatientDetails = this.getAnotherPatientDetails.bind(this);
    }

    componentDidMount() {
        this.getAnotherPatientDetails();
        console.log("med: " + this.state.medicationPlans);
    }

    getAnotherPatientDetails() {
        console.log("path " + this.state.path)
        return PATIENT_API.getAnotherPatientDetails(
            this.state.path,
            (result, status, error) => {
                console.log("medd: " + result.medicationPlans);
                if (result != null && status === 200) {
                    console.log("HERE " + result.patients);
                    this.setState({
                        name: result.name,
                        username: result.username,
                        gender: result.gender,
                        address: result.address,
                        birthDate: result.birthDate,
                        role: result.role,
                        medicationPlans: result.medicationPlans
                    })
                }
            }
        )
    }

    renderHeader() {
        let header = [
            <>
        <th>Id</th>
        <th>Start Date</th> 
        <th>End Date</th>
        <th>Intake Interval</th> 
        </>
    ]
        return header;
    }

    renderTable() {
        return this.state.medicationPlans.map((patient, index) => {
            const {medicationPlanId, startDate, endDate, intakeInterval} = patient
            return (
                <tr key={medicationPlanId}>
                    <Link to={`/medication-plan/details/${medicationPlanId}`}>{medicationPlanId}</Link>
                    <td>{startDate}</td>
                    <td>{endDate}</td>
                    <td>{intakeInterval}</td>
                </tr>
            )
        })
    }

    render() {
        return (
            <>
            <h1><u>Patient Details</u></h1>
            <div>
            <h3>Name: {this.state.name}</h3>
            <h3>Username: {this.state.username}</h3>
            <h3>Gender: {this.state.gender}</h3>
            <h3>Address: {this.state.address}</h3>
            <h3>Birth Date: {this.state.birthDate}</h3>
            <h3>Role: {this.state.role}</h3>
            </div>

            <h2>Medication Plan: </h2>

            <table id='patients'>
                    <tbody>
                        <tr>{this.renderHeader()}</tr>
                        {this.renderTable()}
                    </tbody>
                </table>
            </>
        )
    }

}

export {
    SearchPage,
    AnotherPatientDetails
}