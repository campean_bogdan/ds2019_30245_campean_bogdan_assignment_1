import React from 'react';
import * as DOCTOR_API from '../api/doctor-api';


let path = '';

const SearchPage = ({match, location}) => {
    path = location.pathname;
    return (
        <AnotherDoctorDetails/>
    )
}

class AnotherDoctorDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            username: '',
            gender: '',
            address: '',
            birthDate: '',
            role: '',
            // path = vine ca si request
            path: path
        }

        this.getLoggedDoctorDetails = this.getLoggedDoctorDetails.bind(this);
    }

    componentDidMount() {
        this.getLoggedDoctorDetails();
    }

    getLoggedDoctorDetails() {
        return DOCTOR_API.getAnotherDoctorDetails(
            this.state.path,
            (result, status, error) => {
                if (result != null && status === 200) {
                    this.setState({
                        name: result.name,
                        username: result.username,
                        gender: result.gender,
                        address: result.address,
                        birthDate: result.birthDate,
                        role: result.role
                    })
                }
            }
        )
    }

    render() {
        return (
            <>
            <h1><u>User Details</u></h1>
            <div>
            <h3>Name: {this.state.name}</h3>
            <h3>Username: {this.state.username}</h3>
            <h3>Gender: {this.state.gender}</h3>
            <h3>Address: {this.state.address}</h3>
            <h3>Birth Date: {this.state.birthDate}</h3>
            <h3>Role: {this.state.role}</h3>
            </div>
            </>
        )
    }

}

export {
    SearchPage,
    AnotherDoctorDetails
}