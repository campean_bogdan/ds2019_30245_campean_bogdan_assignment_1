import React from 'react';
import * as DOCTOR_API from './../api/doctor-api';


export class DoctorUserDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            username: '',
            gender: '',
            address: '',
            birthDate: ''
        }

        this.getLoggedDoctorDetails = this.getLoggedDoctorDetails.bind(this);
    }

    componentDidMount() {
        this.getLoggedDoctorDetails();
    }

    getLoggedDoctorDetails() {
        let username = localStorage.getItem('username')
        return DOCTOR_API.getDoctorDetailsByUsername(
            username,
            (result, status, error) => {
                if (result != null && status === 200) {
                    this.setState({
                        name: result.name,
                        username: result.username,
                        gender: result.gender,
                        address: result.address,
                        birthDate: result.birthDate
                    })
                }
            }
        )
    }

    render() {
        return (
            <>
            <h1><u>Your Account Details</u></h1>
            <div>
            <h3>Name: {this.state.name}</h3>
            <h3>Username: {this.state.username}</h3>
            <h3>Gender: {this.state.gender}</h3>
            <h3>Address: {this.state.address}</h3>
            <h3>Birth Date: {this.state.birthDate}</h3>
            </div>
            </>
        )
    }

}