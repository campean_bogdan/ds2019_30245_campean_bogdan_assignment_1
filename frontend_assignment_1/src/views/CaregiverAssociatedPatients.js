import React from 'react';
import * as DOCTOR_API from '../api/doctor-api';
import * as CAREGIVER_API from '../api/caregiver-api';
import * as jwt from '../jwt';
import * as App from '../App';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import LoginPage from './LoginPage';
import '../styles/RegisterPage.css';
import {Redirect} from 'react-router-dom';

import { AllDoctors } from './AllDoctors';
import { DoctorUserDetails } from './DoctorUserDetails';
import InsertUser from './InsertUser.js';
import * as DoctorDetails from './AnotherDoctorDetails'
import * as CaregiverDetails from './AnotherCaregiverDetails';
import * as PatientDetails from './AnotherPatientDetails';
import * as MedicationPlanDetails from './AnotherMedicationPlan'

import { AllCaregivers } from './AllCaregivers';
import { AllPatients } from './AllPatients';
import {EditUser} from './EditUser';
import {AllMedications} from './AllMedications';
import {InsertMedication} from './InsertMedication';
import {InsertMedicationPlan} from './InsertMedicationPlan';
import {DeleteUser} from './DeleteUser';

import '../styles/LinkSection.css'
import { DeleteMedication } from './DeleteMedication';
import { EditMedication } from './EditMedication';
import { AnotherPatientDetails, AnotherMedicationPlan } from './AnotherMedicationPlan';
import { onInsertPatientToCaregiver } from './InsertPatientToCaregiver';
import {CaregiverPage} from './CaregiverPage';


export class CaregiverAssociatedPatients extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            name: '',
            address: '',
            birthDate: '',
            gender: '',
            patients: []
        }

        this.data = [];

        this.getMyPatients = this.getMyPatients.bind(this);
        this.renderTable = this.renderTable.bind(this);
    }

    renderHeader() {
        let header = [
            <>
        <th>Id</th>
        <th>Name</th> 
        <th>Username</th>
        <th>Address</th> 
        <th>Birth Date</th>
        <th>Gender</th>
        </>
    ]
        return header;
    }

    renderTable() {
        return this.state.patients.map((patient, index) => {
            const {id, name, username, address, birthDate, gender} = patient
            return (
                <tr key={id}>
                    <Link to={`/patient/details/${id}`}>{id}</Link>
                    <td>{name}</td>
                    <td>{username}</td>
                    <td>{address}</td>
                    <td>{birthDate}</td>
                    <td>{gender}</td>
                </tr>
            )
        })
    }

    componentWillMount() {
        this.getMyPatients();
    }

    getMyPatients() {
        return CAREGIVER_API.getCaregiverDetails(
            localStorage.getItem('username'),
            (result, status, error) => {
                if (result !== null && status === 200) {
                        this.setState({
                        name: result.name,
                        username: result.username,
                        gender: result.gender,
                        address: result.address,
                        birthDate: result.birthDate,
                        role: result.role,
                        patients: result.patients
                    });
                    this.forceUpdate();
                } else {
                    
                }
            }
        )
    }

    render() {
        
        return (
            <>
                <h1><u>All patients</u></h1>

                <table id='caregivers'>
                    <tbody>
                        <tr>{this.renderHeader()}</tr>
                        {this.renderTable()}
                    </tbody>
                </table>

            </>
        );
    }

    
}