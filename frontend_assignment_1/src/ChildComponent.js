import React from 'react';
import {Component} from 'react';

class ChildComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '',
        }
    }

    sendMessage=()=>{
        const {websocket} = this.props // websocket instance passed as props to the child component.
        console.log('Send message')
        try {
            websocket.send(this.state.data) //send data to the server
        } catch (error) {
            console.log('Error: ' + error) // catch error
        }
    }
    render() {
        return (
            <div>
                what
            </div>
        );
    }
}

export default ChildComponent;