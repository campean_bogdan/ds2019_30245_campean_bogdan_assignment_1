import React from 'react';


let token = 'Default Token';
let type = '';

var loggedInAsDoctor = false;
var loggedInAsCaregiver = false;
var loggedInAsPatient = false;


function init() {
    loggedInAsDoctor = false;
    loggedInAsCaregiver = false;
    loggedInAsPatient = false;
}

function loginAsDoctor() {
    localStorage.setItem('loggedInAsDoctor', true);
    localStorage.setItem('loggedInAsCaregiver', false);
    localStorage.setItem('loggedInAsPatient', false);
}

function isLoggedInAsDoctor() {return localStorage.getItem('loggedInAsDoctor');}

function loginAsCaregiver() {
    localStorage.setItem('loggedInAsDoctor', false);
    localStorage.setItem('loggedInAsCaregiver', true);
    localStorage.setItem('loggedInAsPatient', false);
}

function isLoggedInAsCaregiver() {return localStorage.getItem('loggedInAsCaregiver');}

function loginAsPatient() {
    localStorage.setItem('loggedInAsDoctor', false);
    localStorage.setItem('loggedInAsCaregiver', false);
    localStorage.setItem('loggedInAsPatient', true);
}

function isLoggedInAsPatient() {return localStorage.getItem('loggedInAsPatient');}

function getToken() {
    return token;
}

function setToken(newToken) {
    token = newToken;
}

function logout() {
    localStorage.removeItem('loggedInAsDoctor', false);
    localStorage.removeItem('loggedInAsCaregiver', false);
    localStorage.removeItem('loggedInAsPatient', false);
    localStorage.removeItem('token');
    localStorage.removeItem('username');
    localStorage.removeItem('password');

}

function isLogout() {
    return localStorage.getItem('loggedInAsDoctor') === 'false' &&
        localStorage.getItem('loggedInAsCaregiver') === 'false' &&
        localStorage.getItem('loggedInAsPatient') === 'false'
}

export {
    getToken,
    setToken,
    loginAsDoctor,
    loginAsCaregiver,
    loginAsPatient,
    isLoggedInAsDoctor,
    isLoggedInAsCaregiver,
    isLoggedInAsPatient,
    init,
    logout,
    isLogout
}