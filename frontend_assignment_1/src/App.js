import React from 'react';
import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom';
import {Redirect} from 'react-router-dom';
import './App.css';
import LoginPage from './views/LoginPage';
import RegisterPage from './views/RegisterPage';
import { DoctorsPage } from './views/DoctorsPage';
import { CaregiversPage } from './views/CaregiversPage';
import { PatientsPage } from './views/PatientsPage';
import {Notifications} from './Notifications';
import {Notifications2} from './Notifications2';
import * as jwt from './jwt';


function App() {

  let path = <Redirect to="/"/>
  let showLoginPage = true;

  if (jwt.isLoggedInAsDoctor() === 'true') {
    path = <Redirect to="/doctor"/>
    showLoginPage = false;
  } else
  if (jwt.isLoggedInAsCaregiver() === 'true') {
    path = <Redirect to="/caregiver"/>
    showLoginPage = false;
  } else
  if (jwt.isLoggedInAsPatient() === 'true') {
    path = <Redirect to="/patient"/>
    showLoginPage = false;
  }

  return (
    <Router>
      <section>
        {showLoginPage ? <Link to="/" class="loginButton">Login</Link> : null}
        {showLoginPage ? <Link to="/register" class="registerButton">Register</Link> : null}

        {path}

        <Route exact path="/" component={LoginPage}/>
        <Route exact path="/register" component={RegisterPage}/>
        <Route exact path="/doctor" component={DoctorsPage}/>
        <Route exact path="/caregiver" component={CaregiversPage}/>
        <Route exact path="/patient" component={PatientsPage}/>
        <Route exact path="/caregiver/details-by-username" component={Notifications}/>

      </section>
    </Router>
  )
}

export {
  App
}
