import {ACTIVITY_HOST} from './../host';
import RestApi from './rest-api';
import {getToken} from './../jwt';


var endpoint = {
    getByPatientId: '/activity/all-activities-by-patient/'
}

function getActivitiesByPatientId(path, callback) {
    console.log("PA: " + path)
    let request = new Request(ACTIVITY_HOST.baseUrl + path, {
        method: 'GET'
    })
    console.log(request.url);
    RestApi.executeRequest(request, callback);
}

function getActivitiesByPatientId2(id, callback) {
    let request = new Request(ACTIVITY_HOST.baseUrl + endpoint.getByPatientId + id, {
        method: 'GET'
    })
    console.log(request.url);
    RestApi.executeRequest(request, callback);
}


export {
    getActivitiesByPatientId,
    getActivitiesByPatientId2,
    
}