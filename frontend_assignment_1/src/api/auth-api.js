import {HOST} from './../host';
import RestApi from './rest-api';


const endpoint = {
    login: '/auth/login',
    register: '/auth/signUp',
}

function login(loginForm, callback) {
    let request = new Request(HOST.baseUrl + endpoint.login, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(loginForm)
    });
    RestApi.executeRequest(request, callback);
}

function register(signUpForm, callback) {
    let request = new Request(HOST.baseUrl + endpoint.register, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(signUpForm)
    });
    RestApi.executeRequest(request, callback);
}

export {
    register,
    login
}