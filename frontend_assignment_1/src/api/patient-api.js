import {HOST} from './../host';
import RestApi from './rest-api';
import {getToken} from './../jwt';


var endpoint = {
    allPatients: '/patient/patients?auth=',
    details: '/patient/details-by-username/'
}

function getAllPatients(callback) {
    let request = new Request(HOST.baseUrl + endpoint.allPatients + localStorage.getItem("token"), {
        method: 'GET'
    });
    console.log("Request: " + request);
    RestApi.executeRequest(request, callback);
}

function getAnotherPatientDetails(path, callback) {
    let request = new Request(HOST.baseUrl + path + "?auth=" + localStorage.getItem('token'), {
        method: 'GET'
    })
    console.log(request.url);
    RestApi.executeRequest(request, callback);
}

function getPatientDetails(username, callback) {
    let request = new Request(HOST.baseUrl + endpoint.details + "?username=" + username + "&auth=" + localStorage.getItem('token'), {
        method: 'GET'
    })
    console.log(request.url);
    RestApi.executeRequest(request, callback);
}

export {
    getAllPatients,
    getAnotherPatientDetails,
    getPatientDetails,
}