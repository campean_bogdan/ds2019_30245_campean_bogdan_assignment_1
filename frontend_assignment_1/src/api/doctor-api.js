import {HOST} from './../host';
import RestApi from './rest-api';
import {getToken} from './../jwt';


var endpoint = {
    insert: '/doctor/insert?auth=',
    allDoctors: '/doctor/doctors?auth=',
    detailsById: '/doctor/details-by-id/',
    detailsByUsername: '/doctor/details-by-username/',
    editUser: '/doctor/edit/',
    deleteUser: '/doctor/delete',
}

const token = localStorage.getItem('token')

function getAllDoctors(callback) {
    let request = new Request(HOST.baseUrl + endpoint.allDoctors + token, {
        method: 'GET'
    });
    console.log(request);
    RestApi.executeRequest(request, callback);
}

function getDoctorDetailsById(id, callback) {
    let request = new Request(HOST.baseUrl + endpoint.detailsById + id + '?auth=' + token, {
        method: 'GET'
    })
    console.log(request.url);
    RestApi.executeRequest(request, callback);
}

function getDoctorDetailsByUsername(username, callback) {
    let request = new Request(HOST.baseUrl + endpoint.detailsByUsername + username + '?auth=' + token, {
        method: 'GET'
    })
    console.log(request.url);
    RestApi.executeRequest(request, callback);
}

function insertUser(user, callback) {
    let request = new Request(HOST.baseUrl + endpoint.insert + token, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    })
    console.log("request: " + request.url)
    RestApi.executeRequest(request, callback);
}

function getAnotherDoctorDetails(path, callback) {
    let request = new Request(HOST.baseUrl + path + "?auth=" + token, {
        method: 'GET'
    })
    console.log(request.url);
    RestApi.executeRequest(request, callback);
}

function editUser(user, callback) {
    let request = new Request(HOST.baseUrl + endpoint.editUser + user.username + "?auth=" + token, {
        method: 'PATCH',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    })
    console.log("Request: " + request.url);
    RestApi.executeRequest(request, callback);
}

function deleteUser(username, callback) {
    let request = new Request(HOST.baseUrl + endpoint.deleteUser + "?username=" + username + "&auth=" + token, {
        method: 'DELETE'
    })
    console.log(request.url);
    RestApi.executeRequest(request);
}

export {
    getAllDoctors,
    getDoctorDetailsById,
    getDoctorDetailsByUsername,
    insertUser,
    getAnotherDoctorDetails,
    editUser,
    deleteUser,
}