import {HOST} from './../host';
import RestApi from './rest-api';
import {getToken} from './../jwt';


var endpoint = {
    insert: '/doctor/insert?auth=',
    allCaregivers: '/caregiver/caregivers?auth=',
    details: '/caregiver/details-by-username',
    addPatientToCaregiver: '/caregiver/add-patient'
}

function getAllCaregivers(callback) {
    let request = new Request(HOST.baseUrl + endpoint.allCaregivers + localStorage.getItem("token"), {
        method: 'GET'
    });
    console.log("Request: " + request);
    RestApi.executeRequest(request, callback);
}

function addPatientToCaregiver(caregiverUsername, patientUsername, callback) {
    let request = new Request(HOST.baseUrl + endpoint.addPatientToCaregiver + "?caregiverUsername=" + caregiverUsername + "&patientUsername=" +
        patientUsername + "&auth=" + localStorage.getItem("token"), {
        method: 'PATCH'
    });
    console.log("Request: " + request);
    RestApi.executeRequest(request, callback);
}

function getCaregiverDetails(username, callback) {
    let request = new Request(HOST.baseUrl + endpoint.details + "?username=" + username + '&auth=' + localStorage.getItem('token'), {
        method: 'GET'
    })
    console.log(request.url);
    RestApi.executeRequest(request, callback);
}

function insertUser(user, callback) {
    let request = new Request(HOST.baseUrl + endpoint.insert + localStorage.getItem('token'), {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    })
    console.log("request: " + request.url)
    RestApi.executeRequest(request, callback);
}

function getAnotherCaregiverDetails(path, callback) {
    let request = new Request(HOST.baseUrl + path + "?auth=" + localStorage.getItem('token'), {
        method: 'GET'
    })
    console.log(request.url);
    RestApi.executeRequest(request, callback);
}

export {
    getAllCaregivers,
    getAnotherCaregiverDetails,
    addPatientToCaregiver,
    getCaregiverDetails,
    
}