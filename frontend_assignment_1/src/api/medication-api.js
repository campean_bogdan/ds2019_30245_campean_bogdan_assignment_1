import {HOST} from './../host';
import RestApi from './rest-api';
import {getToken} from './../jwt';


var endpoint = {
    insert: '/medication/insert?auth=',
    allMedications: '/medication/medications?auth=',
    editMedication: '/medication/edit',
    deleteMedication: '/medication/delete',
    addMedicationPlan: '/medication-plan/add-medication',
    details: '/medication-plan/details/',
}

const token = localStorage.getItem('token')

function getAllMedications(callback) {
    let request = new Request(HOST.baseUrl + endpoint.allMedications + token, {
        method: 'GET'
    });
    console.log(request);
    RestApi.executeRequest(request, callback);
}

function insertMedication(medication, callback) {
    let request = new Request(HOST.baseUrl + endpoint.insert + token, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medication)
    })
    console.log("request: " + request.url)
    RestApi.executeRequest(request, callback);
}

function deleteMedication(name, callback) {
    let request = new Request(HOST.baseUrl + endpoint.deleteMedication + "?name=" + name + "&auth=" + token, {
        method: 'DELETE'
    });
    console.log("Request: " + request.url);
    RestApi.executeRequest(request, callback);
}

function editMedication(medication, callback) {
    let request = new Request(HOST.baseUrl + endpoint.editMedication + "?name=" + medication.name + "&auth=" + token, {
        method: 'PATCH',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medication)
    })
    console.log("Request: " + request.url);
    RestApi.executeRequest(request, callback);
}

function addMedicationPlan(username, medicationPlanWithId, callback) {
    let request = new Request(HOST.baseUrl + endpoint.addMedicationPlan + "?username=" + username + "&auth=" + token, {
        method: 'PATCH',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medicationPlanWithId)
    })
    console.log("Request: " + request.url);
    RestApi.executeRequest(request, callback);
}

function getMedicalPlanDetails(path, callback) {
    let request = new Request(HOST.baseUrl + path + "?auth=" + token, {
        method: 'GET'
    })
    console.log("Request: " + request.url);
    RestApi.executeRequest(request, callback);
}

export {
    getAllMedications,
    insertMedication,
    editMedication,
    deleteMedication,
    addMedicationPlan,
    getMedicalPlanDetails,
}