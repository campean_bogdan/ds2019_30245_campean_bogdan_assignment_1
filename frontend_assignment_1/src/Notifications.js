import { Component } from "react";
import React from 'react';
import ChildComponent from './ChildComponent';
import SockJsClient from 'react-stomp';


export class Notifications extends Component {
    constructor(props) {
        super(props);
        this.state = {
            webSocket: null,
        }
    }

    componentDidMount() {
        this.connect();
    }

    timeout = 250;

    connect = () => {
        var ws = new WebSocket('ws://localhost:8082/socket')
        let that = this;
        var connectInterval;

        ws.onopen = () => {
            this.setState({ webSocket: ws });
            that.timeout = 250;
            clearTimeout(connectInterval);
            console.log('Socket onOpen')
        }

        ws.onMessage = evt => {
            console.log('onMessage')
            const message = JSON.parse(evt.data)
            //this.setState({dataFromServer: message})
            console.log('msg: ' + message)
        }

        ws.onclose = e => {
            console.log('Socket onClose')
            console.log(
                `Socket is closed. Reconnect will be attempted in ${Math.min(
                    10000 / 1000,
                    (that.timeout + that.timeout) / 1000
                )} second.`,
                e.reason
            );
            that.timeout = that.timeout + that.timeout;
            connectInterval = setTimeout(this.check, Math.min(10000, that.timeout));
        }

        ws.onerror = err => {
            console.log('Socket onError')
            console.error(
                "Socket encountered error: ",
                err.message,
                "Closing socket"
            );

            ws.close();
        }
    }

    check = () => {
        const { ws } = this.state;
        if (!ws || ws.readyState == WebSocket.CLOSED) this.connect();
    }

    render() {
        //return <ChildComponent webSocket={this.state.webSocket} />
        return <div>Connect</div>
    }
}