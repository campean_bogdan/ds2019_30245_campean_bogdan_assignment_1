package com.assignment3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class ServerConfig {

    @Autowired
    private Environment env;


//    @Bean
//    RemoteExporter registerRMIExporter() {
//        RmiServiceExporter exporter = new RmiServiceExporter();
//        exporter.setServiceName("helloWorldRMI");
//        exporter.setServiceInterface(HelloInterface.class);
//        exporter.setService(new HelloImpl());
//        exporter.setRegistryPort(1099);
//        return exporter;
//    }

//    @Bean(name = "helloService")
//    HelloInterface helloInterface() { return new HelloImpl(); }

//    @Bean
//    public DataSource getDataSource() {
//        DataSource ds = DataSourceBuilder.create()
//                .driverClassName(env.getProperty("com.mysql.jdbc.Driver"))
//                .url("jdbc:mysql://localhost:3306/medical_application?allowPublicKeyRetrieval=true&autoReconnect=true&useSSL=false")
//                .username("root")
//                .password("bobu")
//                .build();
//
//        return ds;
//    }
}
