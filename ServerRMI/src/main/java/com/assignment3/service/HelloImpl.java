package com.assignment3.service;

import com.assignment3.HelloInterface;
import com.assignment3.repository.Repository;

import java.rmi.RemoteException;


public class HelloImpl implements HelloInterface {

    private final Repository repository;


    public HelloImpl(Repository repository) throws RemoteException {
        this.repository = repository;
    }

//    @Override
//    public String sayHello(String msg) {
//        System.out.println("Server got message: " + msg);
//        return "Hello " + msg;
//    }

    @Override
    public String getAll() throws RemoteException {
        repository.getAll();
        return "END";
    }
}
