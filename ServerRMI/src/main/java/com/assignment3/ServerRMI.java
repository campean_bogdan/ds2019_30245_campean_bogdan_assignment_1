package com.assignment3;

import com.assignment3.repository.Repository;
import com.assignment3.repository.RepositoryImpl;
import com.assignment3.service.HelloImpl;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class ServerRMI {

    public static void main(String[] args) {
        //SpringApplication.run(ServerRMI.class, args);
//        SpringApplication app = new SpringApplication(ServerRMI.class);
//        app.setDefaultProperties(Collections.singletonMap("server.port", "8084"));
//        app.run(args);

        //HelloInterface helloService = SpringApplication.run(ClientRMIApp.class, args).getBean("helloInterface", HelloInterface.class);

        Repository repository = new RepositoryImpl();


        try {
            Runtime.getRuntime().exec("rmiregistry 8085");
        	LocateRegistry.createRegistry(8085);
//        String policy = "file:C:\\Users\\student\\CBogdan\\lab7\\src\\lab7\\permissions";
//        System.setProperty("java.security.policy", policy);

            System.setProperty("java.rmi.server.hostname", "localhost");

            HelloImpl obj = new HelloImpl(repository);
            HelloInterface stub = (HelloInterface) UnicastRemoteObject.exportObject(obj, 0);

            String rmi = "rmi://localhost:8085/hello";
            Registry registry = LocateRegistry.createRegistry(8085);
            registry.rebind(rmi, stub);
            //Naming.rebind(rmi, stub);

            System.err.println("Server ready");

        } catch (Exception e) {

            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();

        }

    }
}
