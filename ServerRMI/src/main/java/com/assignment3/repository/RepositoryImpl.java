package com.assignment3.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class RepositoryImpl implements Repository {

    private Connection conn = null;


    @Override
    public String getAll() {
        connect();
        try {
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("select * from medication_plans");

            while (result.next()) {
                System.out.println(result.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void connect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/medical_application?allowPublicKeyRetrieval=true&autoReconnect=true&useSSL=false",
                    "root", "bobu");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
